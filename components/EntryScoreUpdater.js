import React, { Component } from 'react';
import { gql, graphql } from 'react-apollo'
import 'isomorphic-fetch';
import styled from 'styled-components';
import { Default,Button,PrimaryButton,Header,SubHeader,Page,Row,Column,Gradient,Line,Text } from '../uikit';
import axios from 'axios';


import query from '../queries/fetchEntries';

class EntryScoreUpdater extends Component {

  constructor(props) {
    super(props);
    this.state = {
      players: []
    };
  }

  updateNow = () => {
    this.props.data.allEntries.map((entry) => {
      let score1 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick1 });
      let score2 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick2 });
      let score3 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick3 });
      let score4 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick4 });
      let score5 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick5 });
      let score6 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick6 });
      let score7 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick7 });
      let score8 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick8 });
      let score9 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick9 });
      let score10 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick10 });
      let score11 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick11 });
      let score12 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick12 });
      let score13 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick13 });
      let score14 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick14 });
      let score15 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick15 });
      let scores = [
        score1.score,
        score2.score,
        score3.score,
        score4.score,
        score5.score,
        score6.score,
        score7.score,
        score8.score,
        score9.score,
        score10.score,
        score11.score,
        score12.score,
        score13.score,
        score14.score,
        score15.score,
      ];
      //console.log(entry.name + " = ", scores);
      function compareNumbers(a, b) {
        return a - b;
      }
      scores.sort(compareNumbers);
      let totalScore = 0;
      for(let x = 0; x <= 12; x++){
        totalScore = scores[x] + totalScore;
      }
      console.log(entry.name + " = "+ totalScore);
      this.props.mutate({
        variables: {
          "id": entry.id,
          "score": totalScore
        }
      });
    });
  }

  render() {
    if (this.props.data.loading) { return <div>Loading...</div>; }
    return (
      <div>
        <Button onClick={this.updateNow}> UPDATE ENTRY SCORES </Button>
      </div>
    );
  }
}

const mutation = gql`
  mutation  UpdateEntry($id: ID!, $score: Int) {
    updateEntry(
      id: $id
      score: $score
    ) {
      id
    }
  }
`;

export default graphql(mutation)(
  graphql(query)(EntryScoreUpdater)
);
