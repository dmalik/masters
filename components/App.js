import styled, { ThemeProvider } from 'styled-components';
import {Default, Button, PrimaryButton, Header, SubHeader, Page, Row, Column, Gradient, Line, Text} from '../uikit';

export default ({ children }) => (
   <ThemeProvider theme={Default}>
     <Page fluid='true'>
       {children}
     </Page>
   </ThemeProvider>
)
