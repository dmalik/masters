//Include this in one of the pages to create initial player list

import { gql, graphql } from 'react-apollo';
const playerData = require('../initialData/players2018.json');

function AddPlayers ({ createPlayer }) {

  function handleSubmit (e) {
    e.preventDefault()

    playerData.players.map((player) => {
        console.log("player = ", player);
        createPlayer(player.playerId, player.age, player.country, player.firstname, player.group, player.img, player.lastname, player.rank, player.score);
    });


  }

  return (
    <form onSubmit={handleSubmit}>
      <h1>Add Players</h1>
      <button type='submit'>Add Players</button>
    </form>
  )
}

const createPlayer = gql`
  mutation createPlayer(
  $playerId: String!,
  $age: String,
  $country: String,
  $firstname: String,
  $group: String,
  $img: String,
  $lastname: String,
  $rank: String,
  $score: Int) {
  createPlayer(
    playerId: $playerId,
    age: $age,
    country: $country,
  	firstname: $firstname,
  	group: $group,
  	img: $img,
  	lastname: $lastname,
  	rank: $rank,
  	score: $score){
      firstname
    }
}
`

export default graphql(createPlayer, {
  props: ({ mutate }) => ({
    createPlayer: (playerId, age, country, firstname, group, img, lastname, rank, score ) => mutate({
      variables: { playerId, age, country, firstname, group, img, lastname, rank, score },
      updateQueries: {
        allPlayers: (previousResult, { mutationResult }) => {
          const newPlayer = mutationResult.data.createPost
          return Object.assign({}, previousResult, {
            allPosts: [newPlayer, ...previousResult.allPlayers]
          })
        }
      }
    })
  })
})(AddPlayers)
