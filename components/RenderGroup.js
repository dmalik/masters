import React, { Component } from 'react';
import { gql, graphql } from 'react-apollo';
import fetchGroup from '../queries/fetchGroup';

import styled from 'styled-components';
import { Default, Button, PrimaryButton, Header, SubHeader, Page, Row, Column, Gradient, Line, Text } from '../uikit';

const PlayerColumn = styled(Column) `
  color: ${props => props.theme.fontColor};
  font-family: ${props => props.theme.paragraphFont};
  font-size: 16px;
  list-style-type: none;
  text-align: center;
  margin: 1.66666666666666%;
  border-radius: 3px;
  transition: box-shadow .25s;
  border-radius: 2px;
  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
  cursor: pointer;
  transition: all 0.25s linear;
  > label {
    cursor: pointer;
  }
`;

const GroupHeader = styled(Header) `
  margin-top: 1em;
  margin-bottom: 0;
`;

const Flag = styled.img `
  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.14), 0 1px 3px 0 rgba(0,0,0,0.12), 0 2px 1px -2px rgba(0,0,0,0.2);
`;

class RenderGroup extends Component {

  playerClicked = (e) => {
    if(e.target.value != undefined){
      console.log("Click - ", e.target.value);
    }
  }

  groupRender = (item) => {
      return item.map((player) => {
        return (

          <PlayerColumn fluid xs={5} sm={5} md={2} key={player.playerId} onClick={this.playerClicked} >
            <input type="radio" name={"group"+this.props.groupId} value={player.playerId} id={player.playerId} />
            <label htmlFor={player.playerId}>
              <img src={"/static/images/players/"+player.img} width="100%" style={{paddingBottom: "0.3em"}}/>
              <Flag src={"/static/images/flags/"+player.country+".svg"} width="15%" style={{marginBottom: "0.3em"}}/>
              <div>{player.firstname} {player.lastname}</div>
              <div>Age: {player.age}</div>
            </label>
          </PlayerColumn>

        );
      });
  }

  render() {
    if (this.props.loading) { return <div>Loading</div>; }

    return (
      <Row>
        <Column fluid xs={12}>
          <GroupHeader size='140'>Group {this.props.groupId}</GroupHeader>
        </Column>
          {this.groupRender(this.props.data.allPlayers)}
      </Row>
    );
  }
}

export default graphql(fetchGroup, {
  options: (props) => { return { variables: { groupID: props.groupId} } }
})(RenderGroup);
