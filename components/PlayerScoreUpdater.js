import React, { Component } from 'react';
import { gql, graphql } from 'react-apollo'
import 'isomorphic-fetch';
import styled from 'styled-components';
import { Default,Button,PrimaryButton,Header,SubHeader,Page,Row,Column,Gradient,Line,Text } from '../uikit';
import axios from 'axios';


import query from '../queries/fetchPlayers';

class PlayerScoreUpdater extends Component {

  constructor(props) {
    super(props);
    this.state = {
      players: []
    };
  }

  updateNow = () => {
    const request = axios.get(`/updatedata`);
    request.then(({data}) => {
      console.log("Data = ", data.data.player);
      console.log("Props? = ", this.props);
      let mastersData = data.data.player;
      let score = 0;
      //let result = testers.find(function (o) { return o.first_name === "Scott" && o.display_name2 === "Piercy"; });
      mastersData.map((player) => {
        let result = this.props.data.allPlayers.find(function (o) { return o.firstname === player.first_name && o.lastname === player.last_name; });
        if (result === undefined) {
          console.log("Player Error = ",player);
        } else {
          console.log(result.id + " " + result.firstname + " " + result.lastname +" = "+ player.topar);
          if(player.topar === "E") {
            score = 0;
          } else {
            score = Number(player.topar)
          }
          this.props.mutate({
            variables: {
              "id": result.id,
              "score": score
            }
          });
        }

      })
    }).catch(err => {
      console.log("Error = ",errPayload);
    });
  }

  render() {
    if (this.props.data.loading) { return <div>Loading...</div>; }
    return (
      <div>
        <Button onClick={this.updateNow}> UPDATE PLAYER SCORES </Button>
      </div>
    );
  }
}

const mutation = gql`
  mutation  UpdatePlayer($id: ID!, $score: Int) {
  updatePlayer(
    id: $id
    score: $score
  ) {
    id
  }
}
`;

export default graphql(mutation)(
  graphql(query)(PlayerScoreUpdater)
);
