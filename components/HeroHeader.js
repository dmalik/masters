import styled from 'styled-components';
import {Header} from '../uikit';

const HeroHeader = styled(Header)`
  color: white;
  text-align: left;

  @media (max-width: 500px) {
      font-size: ${props => props.size ? Number(props.size) / 1.5: '100'}%;
      text-align: center;
  }

`;

export default HeroHeader;
