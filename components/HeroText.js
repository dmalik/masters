import styled from 'styled-components';
import {Text} from '../uikit';

const HeroText = styled(Text)`
  color: white;
  margin-top: 0;
  text-align: left;

  @media (max-width: 500px) {
      font-size: ${props => props.size ? Number(props.size) / 1.3: '100'}%;
      text-align: center;
  }
`;

export default HeroText;
