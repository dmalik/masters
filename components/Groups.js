import React, { Component } from 'react';
import Router from 'next/router';
import { gql, graphql } from 'react-apollo';
import fetchGroup from '../queries/fetchGroup';
import RenderGroup from './RenderGroup';
import serialize from "form-serialize";
import createEntry from '../mutations/createEntry';

import styled from 'styled-components';
import {Default, Button, PrimaryButton, Header, SubHeader, Page, Row, Column, Gradient, Line, Text, ContainerFloat, FormField, Label, Input} from '../uikit';

let groupIds = [];

//Create an array of 15 for the 15 groups of players we can map through
for (let x = 1; x <= 15; x++ ){
  groupIds.push(x.toString());
}

const EntryColumn = styled(Column) `
  color: ${props => props.theme.fontColor};
  font-family: ${props => props.theme.paragraphFont};
  font-size: 16px;
`;

const EntryInput = styled(Input) `
  color: ${props => props.theme.fontColor};
  font-family: ${props => props.theme.paragraphFont};
  font-size: 16px;
`;


const EntryLabel = styled(Label) `
  background: transparent;
  text-align: left;
  color: ${props => props.theme.fontColor};
  font-family: ${props => props.theme.paragraphFont};
    padding-top: 0;
  font-weight: bold;
`;

const EntryHeader = styled(Header) `
  padding-bottom: 0;
  margin-bottom: 0;
`;

const ErrorText = styled(Text) `
  color: red;
  text-align: center;
`;

const Form = styled.form `
  padding: 1em;
`;


class Groups extends Component {

  constructor(props) {
    super(props);
    this.state = {name: '', email: '', error: false, errorMessage: ""};
  }

  renderGroups = (groupIds) => {
    return groupIds.map((id) => {
      return (
        <RenderGroup groupId={id} key={id} />
      );
    });
  }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  submitForm = (event) => {
    event.preventDefault();
    let form = document.querySelector('#masterspool-form');
    let formData = serialize(form, {hash: true});
    let error = false;
    console.log("Submit! - ", formData);
    for (let y = 1; y <= 15; y++ ){
      let object = "group"+y;
      if(formData[object] === undefined) {
        console.log("Group Error!");
        this.setState({errorMessage: "Submit error! Looks like you missed selecting a player in group "+y+"."});
        error = true;
      }
    }
    if(formData.name === undefined || formData.email === undefined) {
      console.log("Details Error!");
      this.setState({errorMessage: "Submit error! You must enter your name and email address. Scroll up and fill those out."});
      error = true;
    }
    if(error) {
      this.setState({error: true});
    } else {
      let validEmail = this.validateEmail(formData.email);
      if (validEmail){
        this.setState({errorMessage: ""});
        this.setState({error: false});
        console.log("Do Submission");
        this.props.mutate({
          variables: {
            "name": formData.name,
            "email": formData.email,
            "pick1": formData.group1,
            "pick2": formData.group2,
            "pick3": formData.group3,
            "pick4": formData.group4,
            "pick5": formData.group5,
            "pick6": formData.group6,
            "pick7": formData.group7,
            "pick8": formData.group8,
            "pick9": formData.group9,
            "pick10": formData.group10,
            "pick11": formData.group11,
            "pick12": formData.group12,
            "pick13": formData.group13,
            "pick14": formData.group14,
            "pick15": formData.group15
          }
        });
        Router.push('/thanks')
      } else {
        console.log("Email Error!");
        this.setState({errorMessage: "Submit error! You must enter a valid email address. Scroll up and fix that."});
      }
    }
  }

  render() {
    return (
      <Form id="masterspool-form" onSubmit={this.submitForm}>
        <EntryHeader size='140'>Your Entry Details</EntryHeader>
        <EntryColumn xs={10} sm={8} md={6} lg={4}>
          <FormField>
            <EntryLabel>Full Name</EntryLabel>
            <EntryInput name='name' onChange={(e) => {this.setState({name: e.target.value});}} value={this.state.name} />
          </FormField>
          <FormField>
            <EntryLabel>Email</EntryLabel>
            <EntryInput name='email' onChange={(e) => {this.setState({email: e.target.value});}} value={this.state.email} />
          </FormField>
        </EntryColumn>
        {this.renderGroups(groupIds)}
        <Row>
          <Column xs={12} style={{textAlign: 'center'}}>
            {this.state.error ? <ErrorText> {this.state.errorMessage} </ErrorText> : ""}
            <PrimaryButton size='125' type="submit">SUBMIT ENTRY</PrimaryButton>
          </Column>
        </Row>
      </Form>
    );
  }
}

export default graphql(createEntry)(Groups);
