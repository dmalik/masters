import styled from 'styled-components';
import {Row, Gradient} from '../uikit';

const Hero = styled(Row)`
  color: white;
  height: 40vh;
  min-height: 20em;
  background-image: url('static/images/augusta3.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-color: ${props => props.theme.primaryColor ? props.theme.primaryColor : '#4990E2'};
`;

export default Hero;
