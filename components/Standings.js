import React, { Component } from 'react';
import { gql, graphql } from 'react-apollo'
import 'isomorphic-fetch';
import styled from 'styled-components';
import { Default,Button,PrimaryButton,Header,SubHeader,Page,Row,Column,Gradient,Line,Text } from '../uikit';
import axios from 'axios';
import Collapsible from 'react-collapsible';

import query from '../queries/fetchEntries';

class Standings extends Component {

  constructor(props) {
    super(props);
    this.state = {
      players: []
    };
  }

  renderStangings = () => {
    let place = 0;
    //let parsedEntries = [];
    return this.props.data.allEntries.map((entry) => {
      place++;
      let score1 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick1 });
      let score2 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick2 });
      let score3 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick3 });
      let score4 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick4 });
      let score5 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick5 });
      let score6 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick6 });
      let score7 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick7 });
      let score8 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick8 });
      let score9 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick9 });
      let score10 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick10 });
      let score11 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick11 });
      let score12 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick12 });
      let score13 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick13 });
      let score14 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick14 });
      let score15 = this.props.data.allPlayers.find(function (o) { return o.playerId === entry.pick15 });
      /*let picks = [
        { "group": 1, "firstname": score1.firstname, "lastname": score1.lastname, "score": score1.score },
        { "group": 2, "firstname":  score2.firstname, "lastname": score2.lastname, "score": score2.score },
        { "group": 3, "firstname":  score3.firstname, "lastname": score3.lastname, "score": score3.score },
        { "group": 4, "firstname":  score4.firstname, "lastname": score4.lastname, "score": score4.score },
        { "group": 5, "firstname":  score5.firstname, "lastname": score5.lastname, "score": score5.score },
        { "group": 6, "firstname":  score6.firstname, "lastname": score6.lastname, "score": score6.score },
        { "group": 7, "firstname":  score7.firstname, "lastname": score7.lastname, "score": score7.score },
        { "group": 8, "firstname":  score8.firstname, "lastname": score8.lastname, "score": score8.score },
        { "group": 9, "firstname":  score9.firstname, "lastname": score9.lastname, "score": score9.score },
        { "group": 10, "firstname": score10.firstname, "lastname": score10.lastname,"score":  score10.score },
        { "group": 11, "firstname": score11.firstname, "lastname": score11.lastname,"score":  score11.score },
        { "group": 12, "firstname": score12.firstname, "lastname": score12.lastname,"score":  score12.score },
        { "group": 13, "firstname": score13.firstname, "lastname": score13.lastname,"score":  score13.score },
        { "group": 14, "firstname": score14.firstname, "lastname": score14.lastname,"score":  score14.score },
        { "group": 15, "firstname": score15.firstname, "lastname": score15.lastname,"score":  score15.score }
      ];*/

      //parsedEntries.push([place, entry.name, entry.score, picks])
      let title = place + ". " + entry.name + " ( "+ entry.score +" )";
      //console.log("picks = ", picks);
      return (
        <Collapsible trigger={title} key={entry.id}>
          <p>Group 1: {score1.firstname} {score1.lastname} ( {score1.score} )</p>
          <p>Group 2: {score2.firstname} {score2.lastname} ( {score2.score} )</p>
          <p>Group 3: {score3.firstname} {score3.lastname} ( {score3.score} )</p>
          <p>Group 4: {score4.firstname} {score4.lastname} ( {score4.score} )</p>
          <p>Group 5: {score5.firstname} {score5.lastname} ( {score5.score} )</p>
          <p>Group 6: {score6.firstname} {score6.lastname} ( {score6.score} )</p>
          <p>Group 7: {score7.firstname} {score7.lastname} ( {score7.score} )</p>
          <p>Group 8: {score8.firstname} {score8.lastname} ( {score8.score} )</p>
          <p>Group 9: {score9.firstname} {score9.lastname} ( {score9.score} )</p>
          <p>Group 10: {score10.firstname} {score10.lastname} ( {score10.score} )</p>
          <p>Group 11: {score11.firstname} {score11.lastname} ( {score11.score} )</p>
          <p>Group 12: {score12.firstname} {score12.lastname} ( {score12.score} )</p>
          <p>Group 13: {score13.firstname} {score13.lastname} ( {score13.score} )</p>
          <p>Group 14: {score14.firstname} {score14.lastname} ( {score14.score} )</p>
          <p>Group 15: {score15.firstname} {score15.lastname} ( {score15.score} )</p>
        </Collapsible>
      );
    });
  }

  render() {
    if (this.props.data.loading) { return <div>Loading...</div>; }
    return (
      <div>
        {this.renderStangings()}
      </div>
    );
  }
}

export default graphql(query)(Standings);
