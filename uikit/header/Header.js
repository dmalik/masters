import styled from 'styled-components';

/****************
ACCEPTED PROPS
-----------------

size: % (default: 300)
align: string (default: left)

-----------------
*****************/

const Header = styled.h1 `

  color: ${props => props.theme.fontColor ? props.theme.fontColor : '#ff0000'};
  font-family: ${props => props.theme.headerFont ? props.theme.headerFont : 'Work Sans, sans-serif'};
  font-weight: ${props => props.theme.headerFontWeight ? props.theme.headerFontWeight : '300'};
  font-size: ${props => props.size ? props.size : '300'}%;
  margin: ${props => props.size ? 100 / props.size : '0.333333'}em;
  display: block;
  line-height: normal;
  text-align: ${props => props.align ? props.align : 'left'}
  text-transform: uppercase;
`;

export default Header;
