import styled from 'styled-components';
import Header from './Header';

const SubHeader = styled(Header)`

  font-size: ${props => props.size ? props.size : '200'}%;
  margin: ${props => props.size ? 100 / props.size : '0.5'}em;

`;

export default SubHeader;
