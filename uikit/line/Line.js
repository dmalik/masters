import styled from 'styled-components';

const Line = styled.hr `
  background: ${props => props.theme.lineColor ? props.theme.lineColor : '#555'};
  border: 0;
  height: ${props => props.height ? props.height : '1px'};
  margin: 1em;
`;

export default Line;
