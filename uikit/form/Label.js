import styled from 'styled-components';

const Label = styled.label `
  color: #2A3039;
  display: block;
  font-family: '${props => props.theme.headerFont ? props.theme.headerFont : 'sans-serif'}';
  font-size: ${props => props.size ? props.size : '100'}%;
  font-weight: 700;
  position: relative;
  height: 1em;
  margin-bottom: 1em;
  box-sizing: border-box;

`;

export default Label;
