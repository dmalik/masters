import styled from 'styled-components';


const FormField = styled.div `
  position: relative;
  margin-bottom: 2em;

  &:first-child {
    margin-top: 1em;
  }

  &:last-child {
    margin-bottom: 1em;
  }

`;

export default FormField;
