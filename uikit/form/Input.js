import styled from 'styled-components';

const Input = styled.input `

  font-family: '${props => props.theme.paragraphFont ? props.theme.paragraphFont : 'sans-serif'}';
  font-size: ${props => props.size ? props.size : '100'}%;
  font-weight: ${props => props.theme.paragraphFontWeight ? props.theme.paragraphFontWeight : 'normal'};
  background-color: #FFFFFF;
  border: 1px solid #DBE3E7;
  border-radius: 3px;
  color: #536171;
  display: block;
  font-size: 1em;
  transition: all ease-in-out .1s;
  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
  width: 100%;
  height: 3em;
  padding: 0 1em;

  &:focus {
    background-color: #E8F5E9;
    border: 1px solid #43A047;
    outline: 0;
  }

`;

export default Input;
