##Quarry UI Kit
####v0.2.3

###How to use
Quarry UI Kit is intended to be used by copying the entire `uikit/` folder into your project and then importing any assets needed into your layouts. If you make any changes or create any new components that would be beneficial to the UI kit add them in at project completion.

###About
This UI Kit was created to:
- Give us a base set of components and a design language to use
- Speed up design and development
- Create easily maintainable code that is highly reusable
- Make it easy to theme

###Design language (Where things get opinionated)
- There must be a good developer and designer experience when using the UI components
- Components should be designed and adapt to the [8 point grid](https://medium.com/built-to-adapt/intro-to-the-8-point-grid-system-d2573cde8632#.6f5uon7sy). We don’t have to use 8pt but it makes the most sense as it makes it easy to develop for when using rem and em units.
- Components should use rem (Default browser font-size 16px) and then use em units to adapt to different screen sizes.
- Font size should be adjusted using % to make components easily adapt programmatically.
- Components should limit the use of props to only those that are an absolute must to keep things as simple as possible. It should be opinionated to limit bugs.
- Components should be easily themeable
- Components should work well together
- Components should be able to build off of each other
- Components must adhere to accessibility guidelines

###Requirements
Requirements for the components should be minimal as possible and should be able to be used in any net new project without relying on a build system. The base set of components use only *3* libraries which are:
- [React + React DOM (43KB gzip)](https://facebook.github.io/react/)
- [Styled-components (20KB gzip)](https://github.com/styled-components/styled-components)
- [Babel-core (140KB gzip)](https://babeljs.io/) **not* needed for precompiled React app (SPA) or Next.js*

Some components may require other libraries. Those components will mark the size of the libraries needed and where to find them as part of the documentation.

###Contributing
- Make sure to write at minimum a smoke test to make sure that your components are loading correctly.
- Increment version
- Develop in new branch and merge into master
