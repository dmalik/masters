import React from 'react';
import { shallow } from 'enzyme';

//Button
import Button from './button/Button';
import PrimaryButton from './button/PrimaryButton';
//Header
import Header from './header/Header';
import SubHeader from './header/SubHeader';
//flexgrid
import Column from './flexgrid/Column';
import Row from './flexgrid/Row';
import Page from './flexgrid/Page';
import Hidden from './flexgrid/Hidden';
//line
import Line from './line/Line';
//text
import Text from './text/Text';

//Smoke tests

it('button renders without crashing', () => {
  shallow(<Button />);
});

it('primarybutton renders without crashing', () => {
  shallow(<PrimaryButton />);
});

it('header renders without crashing', () => {
  shallow(<Header />);
});

it('subheader renders without crashing', () => {
  shallow(<SubHeader />);
});

it('Column renders without crashing', () => {
  shallow(<Column />);
});

it('Row renders without crashing', () => {
  shallow(<Row />);
});

it('Page renders without crashing', () => {
  shallow(<Page />);
});

it('Hidden renders without crashing', () => {
  shallow(<Hidden />);
});

it('Line renders without crashing', () => {
  shallow(<Line />);
});

it('Text renders without crashing', () => {
  shallow(<Text />);
});
