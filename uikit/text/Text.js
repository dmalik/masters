import styled from 'styled-components';

const Text = styled.p `

  color: ${props => props.theme.fontColor ? props.theme.fontColor : '#a1a1a1'};
  display: block;
  font-family: ${props => props.theme.paragraphFont ? props.theme.paragraphFont : 'sans-serif'};
  font-size: ${props => props.size ? props.size : '100'}%;
  font-weight: ${props => props.theme.paragraphFontWeight ? props.theme.paragraphFontWeight : 'normal'};
  line-height: normal;
  margin: 1em;
  text-align: ${props => props.align ? props.align : 'left'};
  text-transform: normal;
`;

export default Text;
