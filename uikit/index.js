//theme
import Default from './themes/Default';
//button
import Button from './button/Button';
import PrimaryButton from './button/PrimaryButton';
//header
import Header from './header/Header';
import SubHeader from './header/SubHeader';
//flexgrid
import Column from './flexgrid/Column';
import Row from './flexgrid/Row';
import Page from './flexgrid/Page';
import Hidden from './flexgrid/Hidden';
import BreakpointProvider, {withBreakpoints} from './flexgrid/BreakpointProvider';
import { divvy, media, defaultBreakpoints, passOn } from './flexgrid/utils';
//gradientColor
import Gradient from './gradient/Gradient';
//line
import Line from './line/Line';
//text
import Text from './text/Text';
//Forms
import FormField from './form/Field';
import Label from './form/Label';
import Input from './form/Input';

const utils = {
  divvy,
  media,
  defaultBreakpoints,
  passOn,
};

export {
  Default, //theme
  Button, PrimaryButton, //button
  Header, SubHeader, //header
  Column, Page, Row, Hidden, BreakpointProvider, withBreakpoints, utils, //flexgrid
  Gradient, //gradient
  Line, //line
  Text, //line
  FormField, Label, Input //form
};
