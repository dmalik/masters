import styled from 'styled-components';
import Gradient from '../gradient/Gradient';

const Button = styled.button `

  background-color: ${props => props.theme.backgroundColor ? props.theme.backgroundColor : '#fff'};
  border-radius: ${props => props.theme.buttonRounding ? props.theme.buttonRounding : '50px'};
  border: .0777777em solid ${props => props.theme.lineColor ? props.theme.lineColor : '#a1a1a1'};
  box-sizing: border-box;
  color: ${props => props.theme.fontColor ? props.theme.fontColor : '#a1a1a1'};
  cursor: pointer;
  display: inline-block;
  font-family: '${props => props.theme.headerFont ? props.theme.headerFont : 'sans-serif'}';
  font-size: ${props => props.size ? props.size : '100'}%;
  font-weight: 700;
  line-height: normal;
  margin: 0.5em;
  padding: 0.5em 1em;
  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
  text-align: center;
  user-select: none;
  vertical-align: middle;
  white-space: nowrap;
  zoom: 1;
  width: ${props => props.width ? props.width : 'auto'};
  outline: 0;

  &:hover {
    ${Gradient({gradientColor1: 'rgba(0,0,0, 0.07)', gradientColor2: 'rgba(0,0,0, 0.01)', angle: 0})};
  }

  &:active {
    border-color: #000\9;
    box-shadow: 0 0 0 1px rgba(0,0,0, 0.15) inset, 0 0 6px rgba(0,0,0, 0.20) inset;
  }

  &:disabled {
    background-image: none;
    box-shadow: none;
    cursor: not-allowed;
    filter: alpha(opacity=40);
    opacity: 0.40;
    pointer-events: none;
  }
`;

export default Button;
