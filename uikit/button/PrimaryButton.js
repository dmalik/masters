import styled from 'styled-components';
import Button from './Button';

const PrimaryButton = styled(Button)`
  color: white;
  background-color: ${props => props.theme.primaryColor ? props.theme.primaryColor : '#4990E2'};
  border-color: ${props => props.theme.primaryColorDark ? props.theme.primaryColorDark : '#346DB0'};
`;

export default PrimaryButton;
