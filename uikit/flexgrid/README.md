## Flexgrid - Flexbox grid system
Forked from - [hedron](https://github.com/jsbros/hedron)

#### Break Points

Flexgrid comes with 3 built-in break points. These are the break-points that I've found to be most useful in responsive website design.

Flexgrid is built mobile-first, which means that all styles that should be displayed on a mobile device are placed outside of all media queries. Whenever I refer to the xs breakpoint, I'm referring to the default break-point that comes before all media queries.

```
{
  sm: 500,
  md: 768,
  lg: 1100,
}
```
These can be customized using the `<BreakpointProvider>` component.

#### Row Component
``` jsx
import { Row } from 'uikit';
...
<Row />
```
Although this is the most basic usage, you would never use a Row in the above way. In order for a `Row` to be useful, it needs to be filled with `Column` components. Currently it's advised to **only** put `Column` or `Row` components directly inside `Row` components, because of the way the property inheritance system works in flexgrid.

#### Row Options

| Property Name  |      Type     |  Description  |
| -------------: | ------------- | ------------- |
|   alignContent |     String    | Sets the value of the CSS `align-content` property |
|     alignItems |     String    | Sets the value of the CSS `align-items` property |
|      alignSelf |     String    | Sets the value of the CSS `align-self` property |
|          debug |      Bool     | Draws all child columns with "bounding boxes" for easy visualization of the grid. (Default: `false`) |
|      divisions |     Number    | The amount of horizontal columns this row creates. (Default: `12`) |
| justifyContent |     String    | Sets the value of the CSS `justify-content` property |
|          order |     String    | Sets the value of the CSS `order` property |


## Column Component

``` jsx
import { Row, Column } from 'uikit';
...
<Row>
  <Column><p>By default, this renders a div at 100% width.</p></Column>
</Row>
```

### Column Options

| Property Name  |      Type     |  Description  |
| -------------: | ------------- | ------------- |
|          fluid |      Bool     | If true, disable padding. |
|             xs |     Number    | Width during `xs` breakpoint. |
|             sm |     Number    | Width during `sm` breakpoint. |
|             md |     Number    | Width during `md` breakpoint. |
|             lg |     Number    | Width during `lg` breakpoint. |
|        xsShift |     Number    | Width of left margin during `xs` breakpoint. |
|        smShift |     Number    | Width of left margin during `sm` breakpoint. |
|        mdShift |     Number    | Width of left margin during `md` breakpoint. |
|        lgShift |     Number    | Width of left margin during `lg` breakpoint. |


## Layout Example

``` jsx
import { Row, Column } from 'uikit';
...
<Row divisions={2}>
  <Column lg={1}>
    <h1>Site Title</h1>
  </Column>
  <Column lg={1}>
    <h2>Site Slogan</h2>
  </Column>
</Row>

<Row divisions={9}>
  <Column xs={3} lg={1} lgShift={3}>
    <h4>Section Title</h4>
    <p>Etiam pretium libero massa, vitae lacinia nibh ultricies ut.</p>
  </Column>
  <Column xs={3} lg={1}>
    <h4>Section Title</h4>
    <p>Etiam pretium libero massa, vitae lacinia nibh ultricies ut.</p>
  </Column>
  <Column xs={3} lg={1}>
    <h4>Section Title</h4>
    <p>Etiam pretium libero massa, vitae lacinia nibh ultricies ut.</p>
  </Column>
</Row>

<Row>
  <Column sm={4}>
    <h4>Footer Title</h4>
    <p>Etiam pretium libero massa, vitae lacinia nibh ultricies ut.</p>
  </Column>
  <Column sm={8}>
    <ul>
      <li><a href="#">Footer Link</a></li>
      <li><a href="#">Footer Link</a></li>
      <li><a href="#">Footer Link</a></li>
    </ul>
  </Column>
</Row>
```

## BreakpointProvider component

This works similarly of the
`<ThemeProvider>`
component of styled-components.
Any flexgrid component inside `<BreakpointProvider>` will use these breakpoints.

``` jsx
import { BreakpointProvider, Row, Column } from 'uikit';
...
<BreakpointProvider breakpoints={{ sm: 300, md: 568, lg: 900 }}>
  <Row>
    <Column>My breakpoints are different!</Column>
  </Row>
</BreakpointProvider>
```

If any of the breakpoints is not given the default values will be used.

``` jsx
// Here the 'lg' breakpoint defaults to 1100
<BreakpointProvider breakpoints={{ sm: 300, md: 900 }} />
```

Usually you would wrap your entire app into `<BreakpointProvider>` so that you can access the breakpoints anywhere.

## withBreakpoints

What if you need to get the breakpoints in your component? The purpose of `withBreakpoints` is to do exactly that.

Just use this Higher Order Component to get access to the breakpoints as props.

``` jsx
import { withBreakpoints } from 'uikit';

const MyComponent = (props) => <div>Breakpoints: {...props.breakpoints}</div>

export default withBreakpoints(MyComponent);
```

As long as your component is a child of `<BreakpointProvider>`, the `breakpoints` prop will be defined.
