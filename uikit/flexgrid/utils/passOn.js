import React from 'react';
// The passOn function applies a specific set of properties
// to the children components. It will only apply the props
// to a component type that exists in the ofTypes array.
export default function(
  children: React.Children,
  ofTypes: Array<React.Component<>>,
  process: (r: React.Children) => Object = r => r) {
  const response = React.Children.map(children,
    // Check to see if the child's component type is whitelisted,
    // and then process it.
    child => React.isValidElement(child) && ofTypes.includes(child.type)
      ? React.cloneElement(child, process(child))
      : child
  );
  return response;
}
