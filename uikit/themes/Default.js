const Default = {
  //Colors
  backgroundColor: '#fff',
  lineColor: '#135d2b',
  fontColor: '#135d2b',
  primaryColor: '#135d2b',
  primaryColorDark: '#0c401d',
  //Fonts
  paragraphFont: 'Nunito',
  paragraphFontWeight: 'normal',
  headerFont: 'Poppins',
  headerFontWeight: '600',
  //Buttons
  buttonRounding: '3px',
  //Gradients - It's neat to add rgba colors here.
  greyGradient1: '#b5b5b5',
  greyGradient2: '#303030'
};

export default Default;
