## Add gradients to your Components
Forked from [uiGradients](https://github.com/JSBros/uigradients)
###Simple example
You just need to pass in 2 colors.

``` jsx
import styled from 'styled-components';
import {Gradient} from './uikit';

const MyComponent = styled.div`
  ${Gradient({
      gradientColor1: props => props.theme.greyGradient1,
      gradientColor2: props => props.theme.greyGradient2,
  })};
`;

```

Returns a prefixed version of `background-image`

```
    background-image: linear-gradient( -90deg, #b5b5b5, #303030);
```

###Linear example
Add in angle

``` jsx
const MyComponent = styled.div`
  ${Gradient({
      gradientColor1: props => props.theme.greyGradient1,
      gradientColor2: props => props.theme.greyGradient2,
      angle: 150
  })};
`;

```

###Radial example with all options
Add in options

``` jsx
const MyComponent = styled.div`
${Gradient({
    gradientColor1: props => props.theme.greyGradient1,
    gradientColor2: props => props.theme.greyGradient2,
    type: 'radial',
    angle: 150,
    options: {
        position: '50% 10%',
        shape: 'ellipse',
        colorStops: ['0%', '120%'],
        extent: 'farthest-corner'
    }
  })};
`;

```
