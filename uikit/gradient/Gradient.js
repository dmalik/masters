import {css} from 'styled-components';

function generateRadialGradientCss(options,  gradientColor1, gradientColor2) {
  const {shape, position, extent, colorStops} = options;
  return css `
    background-image: -webkit-radial-gradient(
      ${shape} ${extent} at ${position},
      ${gradientColor1} ${colorStops[0]},
      ${gradientColor2} ${colorStops[1]});
    background-image: -moz-radial-gradient(
      ${shape} ${extent} at ${position},
      ${gradientColor1} ${colorStops[0]},
      ${gradientColor2} ${colorStops[1]});
    background-image: -o-radial-gradient(
      ${shape} ${extent} at ${position},
      ${gradientColor1} ${colorStops[0]},
      ${gradientColor2} ${colorStops[1]});
    background-image: radial-gradient(
      ${shape} ${extent} at ${position},
      ${gradientColor1} ${colorStops[0]},
      ${gradientColor2} ${colorStops[1]});
  `;
}

function configRadialGradientOptions(options = {}) {
  const {position, shape, colorStops, extent} = options;
  const radialConfig = {};
  if (position) {
    radialConfig.position = position;
  } else {
    radialConfig.position = 'center';
  }
  if (shape && (shape === 'circle' || shape === 'ellipse')) {
    radialConfig.shape = shape;
  } else {
    radialConfig.shape = 'circle';
  }
  if (Array.isArray(colorStops)) {
    radialConfig.colorStops = colorStops;
  } else {
    radialConfig.colorStops = ['', ''];
  }
  if (extent === 'closest-side' || extent === 'closest-corner' || extent === 'farthest-side' || extent === 'farthest-corner') {
    radialConfig.extent = extent;
  } else {
    radialConfig.extent = '';
  }
  return radialConfig;
}

function Gradient(props = {}) {
  let gradientColor1 = '';
  let gradientColor2 = '';
  let angle = -90;

  if (props.gradientColor1 === undefined) {
    gradientColor1 = '#b5b5b5';
  } else {
    gradientColor1 = props.gradientColor1;
  }
  if (props.gradientColor2 === undefined) {
    gradientColor2 = '#303030';
  } else {
    gradientColor2 = props.gradientColor2;
  }
  if (props.angle !== undefined) {
    angle = props.angle;
  }

  const {type, options} = props;
  if (type === 'radial') {
    const config = configRadialGradientOptions(options);
    return generateRadialGradientCss(config, gradientColor1, gradientColor2);
  }

  return css `
    background-image: -webkit-linear-gradient(
      ${angle}deg,
      ${gradientColor1},
      ${gradientColor2});

    background-image: -moz-linear-gradient(
      ${angle}deg,
      ${gradientColor1},,
      ${gradientColor2});

    background-image: -o-linear-gradient(
      ${angle}deg,
      ${gradientColor1},,
      ${gradientColor2});

    background-image: linear-gradient(
      ${angle}deg,
      ${gradientColor1},
      ${gradientColor2});
  `;
}

export default Gradient;
