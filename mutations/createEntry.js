import gql from 'graphql-tag';

export default gql`
  mutation createEntry(
    $email: String!,
    $name: String,
    $pick1: String,
    $pick2: String,
    $pick3: String,
    $pick4: String,
    $pick5: String,
    $pick6: String,
    $pick7: String,
    $pick8: String,
    $pick9: String,
    $pick10: String,
    $pick11: String,
    $pick12: String,
    $pick13: String,
    $pick14: String,
    $pick15: String) {
    createEntry(
      name: $name,
      email: $email,
      pick1: $pick1,
      pick2: $pick2,
      pick3: $pick3,
      pick4: $pick4,
      pick5: $pick5,
      pick6: $pick6,
      pick7: $pick7,
      pick8: $pick8,
      pick9: $pick9,
      pick10: $pick10,
      pick11: $pick11,
      pick12: $pick12,
      pick13: $pick13,
      pick14: $pick14,
      pick15: $pick15){
        id
        name
      }
    }
`;

/*
{
  		"name": "Persons name",
      "email": "email@email.com",
      "pick1": "123",
      "pick2": "123",
      "pick3": "123",
      "pick4": "123",
      "pick5": "123",
      "pick6": "123",
      "pick7": "123",
      "pick8": "123",
      "pick9": "123",
      "pick10": "123",
      "pick11": "123",
      "pick12": "123",
      "pick13": "123",
      "pick14": "123",
      "pick15": "123"
}

*/
