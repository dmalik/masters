import gql from 'graphql-tag';

export default gql`
  query {
    allEntries(orderBy: score_ASC) {
      id
      name
      paid
      pick1
      pick2
      pick3
      pick4
      pick5
      pick6
      pick7
      pick8
      pick9
      pick10
      pick11
      pick12
      pick13
      pick14
      pick15
      score
    }
    allPlayers{
      id
      firstname
      lastname
      playerId
      score
    }
  }
`;
