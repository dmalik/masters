import gql from 'graphql-tag';

export default gql`
  query($groupID: String!) {
    allPlayers(
      filter: {
        group: $groupID
      }
    ) {
      firstname
      lastname
      age
      country
      playerId
      rank
      img
    }
  }
`;
