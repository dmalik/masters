import gql from 'graphql-tag';

export default gql`
  query {
   	allPlayers{
      id
      firstname
      lastname
      playerId
      score
  	}
  }
`;
