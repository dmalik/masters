'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  mutation createEntry(\n    $email: String!,\n    $name: String,\n    $pick1: String,\n    $pick2: String,\n    $pick3: String,\n    $pick4: String,\n    $pick5: String,\n    $pick6: String,\n    $pick7: String,\n    $pick8: String,\n    $pick9: String,\n    $pick10: String,\n    $pick11: String,\n    $pick12: String,\n    $pick13: String,\n    $pick14: String,\n    $pick15: String) {\n    createEntry(\n      name: $name,\n      email: $email,\n      pick1: $pick1,\n      pick2: $pick2,\n      pick3: $pick3,\n      pick4: $pick4,\n      pick5: $pick5,\n      pick6: $pick6,\n      pick7: $pick7,\n      pick8: $pick8,\n      pick9: $pick9,\n      pick10: $pick10,\n      pick11: $pick11,\n      pick12: $pick12,\n      pick13: $pick13,\n      pick14: $pick14,\n      pick15: $pick15){\n        id\n        name\n      }\n    }\n'], ['\n  mutation createEntry(\n    $email: String!,\n    $name: String,\n    $pick1: String,\n    $pick2: String,\n    $pick3: String,\n    $pick4: String,\n    $pick5: String,\n    $pick6: String,\n    $pick7: String,\n    $pick8: String,\n    $pick9: String,\n    $pick10: String,\n    $pick11: String,\n    $pick12: String,\n    $pick13: String,\n    $pick14: String,\n    $pick15: String) {\n    createEntry(\n      name: $name,\n      email: $email,\n      pick1: $pick1,\n      pick2: $pick2,\n      pick3: $pick3,\n      pick4: $pick4,\n      pick5: $pick5,\n      pick6: $pick6,\n      pick7: $pick7,\n      pick8: $pick8,\n      pick9: $pick9,\n      pick10: $pick10,\n      pick11: $pick11,\n      pick12: $pick12,\n      pick13: $pick13,\n      pick14: $pick14,\n      pick15: $pick15){\n        id\n        name\n      }\n    }\n']);

exports.default = (0, _graphqlTag2.default)(_templateObject);

/*
{
  		"name": "Persons name",
      "email": "email@email.com",
      "pick1": "123",
      "pick2": "123",
      "pick3": "123",
      "pick4": "123",
      "pick5": "123",
      "pick6": "123",
      "pick7": "123",
      "pick8": "123",
      "pick9": "123",
      "pick10": "123",
      "pick11": "123",
      "pick12": "123",
      "pick13": "123",
      "pick14": "123",
      "pick15": "123"
}

*/