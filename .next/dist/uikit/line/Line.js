'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  background: ', ';\n  border: 0;\n  height: ', ';\n  margin: 1em;\n'], ['\n  background: ', ';\n  border: 0;\n  height: ', ';\n  margin: 1em;\n']);

var Line = _styledComponents2.default.hr(_templateObject, function (props) {
  return props.theme.lineColor ? props.theme.lineColor : '#555';
}, function (props) {
  return props.height ? props.height : '1px';
});

exports.default = Line;