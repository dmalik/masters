'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n    background-image: -webkit-radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n    background-image: -moz-radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n    background-image: -o-radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n    background-image: radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n  '], ['\n    background-image: -webkit-radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n    background-image: -moz-radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n    background-image: -o-radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n    background-image: radial-gradient(\n      ', ' ', ' at ', ',\n      ', ' ', ',\n      ', ' ', ');\n  ']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    background-image: -webkit-linear-gradient(\n      ', 'deg,\n      ', ',\n      ', ');\n\n    background-image: -moz-linear-gradient(\n      ', 'deg,\n      ', ',,\n      ', ');\n\n    background-image: -o-linear-gradient(\n      ', 'deg,\n      ', ',,\n      ', ');\n\n    background-image: linear-gradient(\n      ', 'deg,\n      ', ',\n      ', ');\n  '], ['\n    background-image: -webkit-linear-gradient(\n      ', 'deg,\n      ', ',\n      ', ');\n\n    background-image: -moz-linear-gradient(\n      ', 'deg,\n      ', ',,\n      ', ');\n\n    background-image: -o-linear-gradient(\n      ', 'deg,\n      ', ',,\n      ', ');\n\n    background-image: linear-gradient(\n      ', 'deg,\n      ', ',\n      ', ');\n  ']);

function generateRadialGradientCss(options, gradientColor1, gradientColor2) {
  var shape = options.shape,
      position = options.position,
      extent = options.extent,
      colorStops = options.colorStops;

  return (0, _styledComponents.css)(_templateObject, shape, extent, position, gradientColor1, colorStops[0], gradientColor2, colorStops[1], shape, extent, position, gradientColor1, colorStops[0], gradientColor2, colorStops[1], shape, extent, position, gradientColor1, colorStops[0], gradientColor2, colorStops[1], shape, extent, position, gradientColor1, colorStops[0], gradientColor2, colorStops[1]);
}

function configRadialGradientOptions() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var position = options.position,
      shape = options.shape,
      colorStops = options.colorStops,
      extent = options.extent;

  var radialConfig = {};
  if (position) {
    radialConfig.position = position;
  } else {
    radialConfig.position = 'center';
  }
  if (shape && (shape === 'circle' || shape === 'ellipse')) {
    radialConfig.shape = shape;
  } else {
    radialConfig.shape = 'circle';
  }
  if (Array.isArray(colorStops)) {
    radialConfig.colorStops = colorStops;
  } else {
    radialConfig.colorStops = ['', ''];
  }
  if (extent === 'closest-side' || extent === 'closest-corner' || extent === 'farthest-side' || extent === 'farthest-corner') {
    radialConfig.extent = extent;
  } else {
    radialConfig.extent = '';
  }
  return radialConfig;
}

function Gradient() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var gradientColor1 = '';
  var gradientColor2 = '';
  var angle = -90;

  if (props.gradientColor1 === undefined) {
    gradientColor1 = '#b5b5b5';
  } else {
    gradientColor1 = props.gradientColor1;
  }
  if (props.gradientColor2 === undefined) {
    gradientColor2 = '#303030';
  } else {
    gradientColor2 = props.gradientColor2;
  }
  if (props.angle !== undefined) {
    angle = props.angle;
  }

  var type = props.type,
      options = props.options;

  if (type === 'radial') {
    var config = configRadialGradientOptions(options);
    return generateRadialGradientCss(config, gradientColor1, gradientColor2);
  }

  return (0, _styledComponents.css)(_templateObject2, angle, gradientColor1, gradientColor2, angle, gradientColor1, gradientColor2, angle, gradientColor1, gradientColor2, angle, gradientColor1, gradientColor2);
}

exports.default = Gradient;