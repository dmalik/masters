'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Button = require('./Button');

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: white;\n  background-color: ', ';\n  border-color: ', ';\n'], ['\n  color: white;\n  background-color: ', ';\n  border-color: ', ';\n']);

var PrimaryButton = (0, _styledComponents2.default)(_Button2.default)(_templateObject, function (props) {
  return props.theme.primaryColor ? props.theme.primaryColor : '#4990E2';
}, function (props) {
  return props.theme.primaryColorDark ? props.theme.primaryColorDark : '#346DB0';
});

exports.default = PrimaryButton;