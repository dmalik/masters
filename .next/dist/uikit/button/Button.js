'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Gradient = require('../gradient/Gradient');

var _Gradient2 = _interopRequireDefault(_Gradient);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n\n  background-color: ', ';\n  border-radius: ', ';\n  border: .0777777em solid ', ';\n  box-sizing: border-box;\n  color: ', ';\n  cursor: pointer;\n  display: inline-block;\n  font-family: \'', '\';\n  font-size: ', '%;\n  font-weight: 700;\n  line-height: normal;\n  margin: 0.5em;\n  padding: 0.5em 1em;\n  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\n  text-align: center;\n  user-select: none;\n  vertical-align: middle;\n  white-space: nowrap;\n  zoom: 1;\n  width: ', ';\n  outline: 0;\n\n  &:hover {\n    ', ';\n  }\n\n  &:active {\n    border-color: #0009;\n    box-shadow: 0 0 0 1px rgba(0,0,0, 0.15) inset, 0 0 6px rgba(0,0,0, 0.20) inset;\n  }\n\n  &:disabled {\n    background-image: none;\n    box-shadow: none;\n    cursor: not-allowed;\n    filter: alpha(opacity=40);\n    opacity: 0.40;\n    pointer-events: none;\n  }\n'], ['\n\n  background-color: ', ';\n  border-radius: ', ';\n  border: .0777777em solid ', ';\n  box-sizing: border-box;\n  color: ', ';\n  cursor: pointer;\n  display: inline-block;\n  font-family: \'', '\';\n  font-size: ', '%;\n  font-weight: 700;\n  line-height: normal;\n  margin: 0.5em;\n  padding: 0.5em 1em;\n  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\n  text-align: center;\n  user-select: none;\n  vertical-align: middle;\n  white-space: nowrap;\n  zoom: 1;\n  width: ', ';\n  outline: 0;\n\n  &:hover {\n    ', ';\n  }\n\n  &:active {\n    border-color: #000\\9;\n    box-shadow: 0 0 0 1px rgba(0,0,0, 0.15) inset, 0 0 6px rgba(0,0,0, 0.20) inset;\n  }\n\n  &:disabled {\n    background-image: none;\n    box-shadow: none;\n    cursor: not-allowed;\n    filter: alpha(opacity=40);\n    opacity: 0.40;\n    pointer-events: none;\n  }\n']);

var Button = _styledComponents2.default.button(_templateObject, function (props) {
  return props.theme.backgroundColor ? props.theme.backgroundColor : '#fff';
}, function (props) {
  return props.theme.buttonRounding ? props.theme.buttonRounding : '50px';
}, function (props) {
  return props.theme.lineColor ? props.theme.lineColor : '#a1a1a1';
}, function (props) {
  return props.theme.fontColor ? props.theme.fontColor : '#a1a1a1';
}, function (props) {
  return props.theme.headerFont ? props.theme.headerFont : 'sans-serif';
}, function (props) {
  return props.size ? props.size : '100';
}, function (props) {
  return props.width ? props.width : 'auto';
}, (0, _Gradient2.default)({ gradientColor1: 'rgba(0,0,0, 0.07)', gradientColor2: 'rgba(0,0,0, 0.01)', angle: 0 }));

exports.default = Button;