'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  position: relative;\n  margin-bottom: 2em;\n\n  &:first-child {\n    margin-top: 1em;\n  }\n\n  &:last-child {\n    margin-bottom: 1em;\n  }\n\n'], ['\n  position: relative;\n  margin-bottom: 2em;\n\n  &:first-child {\n    margin-top: 1em;\n  }\n\n  &:last-child {\n    margin-bottom: 1em;\n  }\n\n']);

var FormField = _styledComponents2.default.div(_templateObject);

exports.default = FormField;