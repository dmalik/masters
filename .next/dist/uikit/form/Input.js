'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n\n  font-family: \'', '\';\n  font-size: ', '%;\n  font-weight: ', ';\n  background-color: #FFFFFF;\n  border: 1px solid #DBE3E7;\n  border-radius: 3px;\n  color: #536171;\n  display: block;\n  font-size: 1em;\n  transition: all ease-in-out .1s;\n  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\n  width: 100%;\n  height: 3em;\n  padding: 0 1em;\n\n  &:focus {\n    background-color: #E8F5E9;\n    border: 1px solid #43A047;\n    outline: 0;\n  }\n\n'], ['\n\n  font-family: \'', '\';\n  font-size: ', '%;\n  font-weight: ', ';\n  background-color: #FFFFFF;\n  border: 1px solid #DBE3E7;\n  border-radius: 3px;\n  color: #536171;\n  display: block;\n  font-size: 1em;\n  transition: all ease-in-out .1s;\n  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\n  width: 100%;\n  height: 3em;\n  padding: 0 1em;\n\n  &:focus {\n    background-color: #E8F5E9;\n    border: 1px solid #43A047;\n    outline: 0;\n  }\n\n']);

var Input = _styledComponents2.default.input(_templateObject, function (props) {
  return props.theme.paragraphFont ? props.theme.paragraphFont : 'sans-serif';
}, function (props) {
  return props.size ? props.size : '100';
}, function (props) {
  return props.theme.paragraphFontWeight ? props.theme.paragraphFontWeight : 'normal';
});

exports.default = Input;