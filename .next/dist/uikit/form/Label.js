'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: #2A3039;\n  display: block;\n  font-family: \'', '\';\n  font-size: ', '%;\n  font-weight: 700;\n  position: relative;\n  height: 1em;\n  margin-bottom: 1em;\n  box-sizing: border-box;\n\n'], ['\n  color: #2A3039;\n  display: block;\n  font-family: \'', '\';\n  font-size: ', '%;\n  font-weight: 700;\n  position: relative;\n  height: 1em;\n  margin-bottom: 1em;\n  box-sizing: border-box;\n\n']);

var Label = _styledComponents2.default.label(_templateObject, function (props) {
  return props.theme.headerFont ? props.theme.headerFont : 'sans-serif';
}, function (props) {
  return props.size ? props.size : '100';
});

exports.default = Label;