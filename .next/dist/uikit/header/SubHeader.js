'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Header = require('./Header');

var _Header2 = _interopRequireDefault(_Header);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n\n  font-size: ', '%;\n  margin: ', 'em;\n\n'], ['\n\n  font-size: ', '%;\n  margin: ', 'em;\n\n']);

var SubHeader = (0, _styledComponents2.default)(_Header2.default)(_templateObject, function (props) {
  return props.size ? props.size : '200';
}, function (props) {
  return props.size ? 100 / props.size : '0.5';
});

exports.default = SubHeader;