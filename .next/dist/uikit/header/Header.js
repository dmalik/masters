'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n\n  color: ', ';\n  font-family: ', ';\n  font-weight: ', ';\n  font-size: ', '%;\n  margin: ', 'em;\n  display: block;\n  line-height: normal;\n  text-align: ', '\n  text-transform: uppercase;\n'], ['\n\n  color: ', ';\n  font-family: ', ';\n  font-weight: ', ';\n  font-size: ', '%;\n  margin: ', 'em;\n  display: block;\n  line-height: normal;\n  text-align: ', '\n  text-transform: uppercase;\n']);

/****************
ACCEPTED PROPS
-----------------

size: % (default: 300)
align: string (default: left)

-----------------
*****************/

var Header = _styledComponents2.default.h1(_templateObject, function (props) {
  return props.theme.fontColor ? props.theme.fontColor : '#ff0000';
}, function (props) {
  return props.theme.headerFont ? props.theme.headerFont : 'Work Sans, sans-serif';
}, function (props) {
  return props.theme.headerFontWeight ? props.theme.headerFontWeight : '300';
}, function (props) {
  return props.size ? props.size : '300';
}, function (props) {
  return props.size ? 100 / props.size : '0.333333';
}, function (props) {
  return props.align ? props.align : 'left';
});

exports.default = Header;