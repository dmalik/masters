'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n\n  color: ', ';\n  display: block;\n  font-family: ', ';\n  font-size: ', '%;\n  font-weight: ', ';\n  line-height: normal;\n  margin: 1em;\n  text-align: ', ';\n  text-transform: normal;\n'], ['\n\n  color: ', ';\n  display: block;\n  font-family: ', ';\n  font-size: ', '%;\n  font-weight: ', ';\n  line-height: normal;\n  margin: 1em;\n  text-align: ', ';\n  text-transform: normal;\n']);

var Text = _styledComponents2.default.p(_templateObject, function (props) {
  return props.theme.fontColor ? props.theme.fontColor : '#a1a1a1';
}, function (props) {
  return props.theme.paragraphFont ? props.theme.paragraphFont : 'sans-serif';
}, function (props) {
  return props.size ? props.size : '100';
}, function (props) {
  return props.theme.paragraphFontWeight ? props.theme.paragraphFontWeight : 'normal';
}, function (props) {
  return props.align ? props.align : 'left';
});

exports.default = Text;