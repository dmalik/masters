'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Column = require('./Column');

var _Column2 = _interopRequireDefault(_Column);

var _Hidden = require('./Hidden');

var _Hidden2 = _interopRequireDefault(_Hidden);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  ', '\n  ', '\n  ', '\n  ', '\n  ', '\n  ', '\n'], ['\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  ', '\n  ', '\n  ', '\n  ', '\n  ', '\n  ', '\n']);

/* eslint-disable no-unused-vars */


function RowContainer(props) {
  var children = props.children,
      tagName = props.tagName,
      debug = props.debug,
      divisions = props.divisions,
      theme = props.theme,
      alignContent = props.alignContent,
      alignItems = props.alignItems,
      alignSelf = props.alignSelf,
      justifyContent = props.justifyContent,
      order = props.order,
      height = props.height,
      rest = (0, _objectWithoutProperties3.default)(props, ['children', 'tagName', 'debug', 'divisions', 'theme', 'alignContent', 'alignItems', 'alignSelf', 'justifyContent', 'order', 'height']);

  var newChildren = (0, _utils.passOn)(children, [_Column2.default, _Hidden2.default], function (child) {
    return {
      debug: typeof child.props.debug === 'undefined' ? debug : child.props.debug,
      divisions: divisions
    };
  });
  return _react2.default.createElement(tagName || 'section', rest, newChildren);
}

RowContainer.defaultProps = {
  divisions: 12
};

var Row = (0, _styledComponents2.default)(RowContainer)(_templateObject, function (props) {
  return props.alignContent ? 'align-content: ' + props.alignContent + ';' : null;
}, function (props) {
  return props.height ? 'height: ' + props.height + ';' : null;
}, function (props) {
  return props.alignItems ? 'align-items: ' + props.alignItems + ';' : null;
}, function (props) {
  return props.alignSelf ? 'align-self: ' + props.alignSelf + ';' : null;
}, function (props) {
  return props.justifyContent ? 'justify-content: ' + props.justifyContent + ';' : null;
}, function (props) {
  return props.order ? 'order: ' + props.order + ';' : null;
});

exports.default = Row;