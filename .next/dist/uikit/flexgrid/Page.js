'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Row = require('./Row');

var _Row2 = _interopRequireDefault(_Row);

var _Hidden = require('./Hidden');

var _Hidden2 = _interopRequireDefault(_Hidden);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  ', '\n'], ['\n  ', '\n']);

/* eslint-disable no-unused-vars */


function PageContainer(props) {
  var children = props.children,
      tagName = props.tagName,
      debug = props.debug,
      fluid = props.fluid,
      rest = (0, _objectWithoutProperties3.default)(props, ['children', 'tagName', 'debug', 'fluid']);

  var newChildren = (0, _utils.passOn)(children, [_Row2.default, _Hidden2.default], function (child) {
    return {
      debug: typeof child.props.debug === 'undefined' ? debug : child.props.debug
    };
  });
  return _react2.default.createElement(tagName || 'div', rest, newChildren);
}

var Page = (0, _styledComponents2.default)(PageContainer)(_templateObject, function (props) {
  return props.fluid ? 'width: 100%;' : 'margin: 0 auto; max-width: 100%; ' + (props.width ? 'width: ' + props.width + ';' : 'width: 960px;') + '\n  ';
});

exports.default = Page;