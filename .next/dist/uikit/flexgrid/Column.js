'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Row = require('./Row');

var _Row2 = _interopRequireDefault(_Row);

var _BreakpointProvider = require('./BreakpointProvider');

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  display: block;\n  ', '\n  box-sizing: border-box;\n  ', '\n  width: 100%;\n  ', '\n  ', '\n  ', '\n  ', '\n  ', '\n'], ['\n  display: block;\n  ', '\n  box-sizing: border-box;\n  ', '\n  width: 100%;\n  ', '\n  ', '\n  ', '\n  ', '\n  ', '\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n    ', '\n    ', '\n  '], ['\n    ', '\n    ', '\n  ']);

/* eslint-disable no-unused-vars */


function ColumnContainer(props) {
  var children = props.children,
      tagName = props.tagName,
      debug = props.debug,
      divisions = props.divisions,
      fluid = props.fluid,
      xs = props.xs,
      sm = props.sm,
      md = props.md,
      lg = props.lg,
      theme = props.theme,
      xsShift = props.xsShift,
      smShift = props.smShift,
      mdShift = props.mdShift,
      lgShift = props.lgShift,
      breakpoints = props.breakpoints,
      rest = (0, _objectWithoutProperties3.default)(props, ['children', 'tagName', 'debug', 'divisions', 'fluid', 'xs', 'sm', 'md', 'lg', 'theme', 'xsShift', 'smShift', 'mdShift', 'lgShift', 'breakpoints']);

  var newChildren = (0, _utils.passOn)(children, [_Row2.default], function (child) {
    return {
      debug: typeof child.props.debug === 'undefined' ? debug : child.props.debug
    };
  });
  return _react2.default.createElement(tagName || 'div', rest, newChildren);
}

ColumnContainer.defaultProps = {
  divisions: 12
};

var Column = (0, _styledComponents2.default)(ColumnContainer)(_templateObject, function (props) {
  return props.debug ? 'background-color: rgba(50, 50, 255, .1);\n  border: 1px solid #fff;' : '';
}, function (props) {
  return props.fluid ? 'padding: 0;' : 'padding: 1em;';
}, function (props) {
  return props.xs ? 'width: ' + (0, _utils.divvy)(props.divisions, props.xs) + '%;' : null;
}, function (props) {
  return props.xsShift ? 'margin-left: ' + (0, _utils.divvy)(props.divisions, props.xsShift) + '%;' : null;
}, function (_ref) {
  var breakpoints = _ref.breakpoints;
  return _utils.media.sm(breakpoints)(_templateObject2, function (props) {
    return props.sm ? 'width: ' + (0, _utils.divvy)(props.divisions, props.sm) + '%;' : null;
  }, function (props) {
    return props.smShift ? 'margin-left: ' + (0, _utils.divvy)(props.divisions, props.smShift) + '%;' : null;
  });
}, function (_ref2) {
  var breakpoints = _ref2.breakpoints;
  return _utils.media.md(breakpoints)(_templateObject2, function (props) {
    return props.md ? 'width: ' + (0, _utils.divvy)(props.divisions, props.md) + '%;' : null;
  }, function (props) {
    return props.mdShift ? 'margin-left: ' + (0, _utils.divvy)(props.divisions, props.mdShift) + '%;' : null;
  });
}, function (_ref3) {
  var breakpoints = _ref3.breakpoints;
  return _utils.media.lg(breakpoints)(_templateObject2, function (props) {
    return props.lg ? 'width: ' + (0, _utils.divvy)(props.divisions, props.lg) + '%;' : null;
  }, function (props) {
    return props.lgShift ? 'margin-left: ' + (0, _utils.divvy)(props.divisions, props.lgShift) + '%;' : null;
  });
});

exports.default = (0, _BreakpointProvider.withBreakpoints)(Column);