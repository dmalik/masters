'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _Row = require('./Row');

var _Row2 = _interopRequireDefault(_Row);

var _Column = require('./Column');

var _Column2 = _interopRequireDefault(_Column);

var _BreakpointProvider = require('./BreakpointProvider');

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  ', '\n  ', '\n  ', '\n  ', '\n'], ['\n  ', '\n  ', '\n  ', '\n  ', '\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)([' $ {\n      props => props.sm\n        ? \'display: none;\'\n        : \'display: inherit;\'\n    }\n    '], [' $ {\n      props => props.sm\n        ? \'display: none;\'\n        : \'display: inherit;\'\n    }\n    ']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)([' $ {\n        props => props.md\n          ? \'display: none;\'\n          : \'display: inherit;\'\n      }\n      '], [' $ {\n        props => props.md\n          ? \'display: none;\'\n          : \'display: inherit;\'\n      }\n      ']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)([' $ {\n          props => props.lg\n            ? \'display: none;\'\n            : \'display: inherit;\'\n        }\n        '], [' $ {\n          props => props.lg\n            ? \'display: none;\'\n            : \'display: inherit;\'\n        }\n        ']);

/* eslint-disable no-unused-vars */


function HiddenContainer(props) {
  var children = props.children,
      debug = props.debug,
      xs = props.xs,
      sm = props.sm,
      md = props.md,
      lg = props.lg,
      breakpoints = props.breakpoints,
      rest = (0, _objectWithoutProperties3.default)(props, ['children', 'debug', 'xs', 'sm', 'md', 'lg', 'breakpoints']);

  var newChildren = (0, _utils.passOn)(children, [_Row2.default, _Column2.default], function (child) {
    return {
      debug: typeof child.props.debug === 'undefined' ? debug : child.props.debug
    };
  });
  return _react2.default.createElement('div', rest, newChildren);
}

var Hidden = (0, _styledComponents2.default)(HiddenContainer)(_templateObject, function (props) {
  return props.xs ? 'display: none;' : 'display: inherit;';
}, function (_ref) {
  var breakpoints = _ref.breakpoints;
  return _utils.media.sm(breakpoints)(_templateObject2);
}, function (_ref2) {
  var breakpoints = _ref2.breakpoints;
  return _utils.media.md(breakpoints)(_templateObject3);
}, function (_ref3) {
  var breakpoints = _ref3.breakpoints;
  return _utils.media.lg(breakpoints)(_templateObject4);
});

exports.default = (0, _BreakpointProvider.withBreakpoints)(Hidden);