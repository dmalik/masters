'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withBreakpoints = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var breakpointsShape = _react.PropTypes.shape({ sm: _react.PropTypes.number, md: _react.PropTypes.number, lg: _react.PropTypes.number });

var BreakpointProvider = function (_Component) {
  (0, _inherits3.default)(BreakpointProvider, _Component);

  function BreakpointProvider() {
    (0, _classCallCheck3.default)(this, BreakpointProvider);

    return (0, _possibleConstructorReturn3.default)(this, (BreakpointProvider.__proto__ || (0, _getPrototypeOf2.default)(BreakpointProvider)).apply(this, arguments));
  }

  (0, _createClass3.default)(BreakpointProvider, [{
    key: 'getChildContext',
    value: function getChildContext() {
      var _props$breakpoints = this.props.breakpoints,
          propsBreakpoints = _props$breakpoints === undefined ? {} : _props$breakpoints;
      var _context$breakpoints = this.context.breakpoints,
          contextBreakpoints = _context$breakpoints === undefined ? {} : _context$breakpoints;

      return {
        breakpoints: (0, _extends3.default)({}, _utils.defaultBreakpoints, contextBreakpoints, propsBreakpoints)
      };
    }
  }, {
    key: 'render',
    value: function render() {
      return _react.Children.only(this.props.children);
    }
  }]);

  return BreakpointProvider;
}(_react.Component);

BreakpointProvider.contextTypes = {
  breakpoints: breakpointsShape
};
BreakpointProvider.childContextTypes = {
  breakpoints: breakpointsShape
};
exports.default = BreakpointProvider;
var withBreakpoints = exports.withBreakpoints = function withBreakpoints(WrappedComponent) {
  var _class, _temp;

  return (
    // eslint-disable-next-line react/no-multi-comp
    _temp = _class = function (_Component2) {
      (0, _inherits3.default)(Breakpoints, _Component2);

      function Breakpoints() {
        (0, _classCallCheck3.default)(this, Breakpoints);

        return (0, _possibleConstructorReturn3.default)(this, (Breakpoints.__proto__ || (0, _getPrototypeOf2.default)(Breakpoints)).apply(this, arguments));
      }

      (0, _createClass3.default)(Breakpoints, [{
        key: 'render',
        value: function render() {
          var breakpoints = this.context.breakpoints;

          return _react2.default.createElement(WrappedComponent, (0, _extends3.default)({}, this.props, { breakpoints: breakpoints }));
        } // eslint-disable-line  react/prefer-stateless-function

      }]);

      return Breakpoints;
    }(_react.Component), _class.contextTypes = {
      breakpoints: breakpointsShape
    }, _temp
  );
};