'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultBreakpoints = undefined;

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  @media (min-width: ', 'px) {\n    ', '\n  }'], ['\n  @media (min-width: ', 'px) {\n    ', '\n  }']);

var defaultBreakpoints = exports.defaultBreakpoints = {
  sm: 500,
  md: 768,
  lg: 1100
};

var query = function query(size) {
  var breakpoints = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultBreakpoints;
  return function () {
    return (0, _styledComponents.css)(_templateObject, breakpoints[size] || defaultBreakpoints[size], _styledComponents.css.apply(undefined, arguments));
  };
};

exports.default = (0, _keys2.default)(defaultBreakpoints).reduce(function (acc, label) {
  var accumulator = acc;
  accumulator[label] = function (breakpoints) {
    return query(label, breakpoints);
  };
  return accumulator;
}, {});