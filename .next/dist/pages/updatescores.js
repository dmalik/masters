'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withData = require('../lib/withData');

var _withData2 = _interopRequireDefault(_withData);

var _head = require('next/dist/lib/head.js');

var _head2 = _interopRequireDefault(_head);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

var _App = require('../components/App');

var _App2 = _interopRequireDefault(_App);

var _PlayerScoreUpdater = require('../components/PlayerScoreUpdater');

var _PlayerScoreUpdater2 = _interopRequireDefault(_PlayerScoreUpdater);

var _EntryScoreUpdater = require('../components/EntryScoreUpdater');

var _EntryScoreUpdater2 = _interopRequireDefault(_EntryScoreUpdater);

require('isomorphic-fetch');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _withData2.default)(function (props) {
  return _react2.default.createElement('div', null, _react2.default.createElement(_head2.default, null, _react2.default.createElement('title', null, 'Masters Pool 2018 - Update Scores'), _react2.default.createElement('meta', { charSet: 'utf-8' }), _react2.default.createElement('meta', { name: 'viewport', content: 'initial-scale=1.0, width=device-width' })), _react2.default.createElement(_App2.default, null, _react2.default.createElement(_uikit.Column, { xs: 12 }, _react2.default.createElement(_PlayerScoreUpdater2.default, null), _react2.default.createElement(_EntryScoreUpdater2.default, null))));
});