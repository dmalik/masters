'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withData = require('../lib/withData');

var _withData2 = _interopRequireDefault(_withData);

var _head = require('next/dist/lib/head.js');

var _head2 = _interopRequireDefault(_head);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

var _App = require('../components/App');

var _App2 = _interopRequireDefault(_App);

var _AddPlayers = require('../components/AddPlayers');

var _AddPlayers2 = _interopRequireDefault(_AddPlayers);

var _Hero = require('../components/Hero');

var _Hero2 = _interopRequireDefault(_Hero);

var _HeroHeader = require('../components/HeroHeader');

var _HeroHeader2 = _interopRequireDefault(_HeroHeader);

var _HeroText = require('../components/HeroText');

var _HeroText2 = _interopRequireDefault(_HeroText);

var _RenderGroup = require('../components/RenderGroup');

var _RenderGroup2 = _interopRequireDefault(_RenderGroup);

var _Groups = require('../components/Groups');

var _Groups2 = _interopRequireDefault(_Groups);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: bullet;\n  text-align: left;\n'], ['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: bullet;\n  text-align: left;\n']);

var List = _styledComponents2.default.ul(_templateObject, function (props) {
  return props.theme.fontColor;
}, function (props) {
  return props.theme.paragraphFont;
});

exports.default = (0, _withData2.default)(function (props) {
  return _react2.default.createElement('div', null, _react2.default.createElement(_head2.default, null, _react2.default.createElement('title', null, 'Masters Pool 2018'), _react2.default.createElement('meta', { charSet: 'utf-8' }), _react2.default.createElement('meta', { name: 'viewport', content: 'initial-scale=1.0, width=device-width' })), _react2.default.createElement(_App2.default, null, _react2.default.createElement(_Hero2.default, { alignItems: 'center' }, _react2.default.createElement(_uikit.Column, { xs: 5 }, _react2.default.createElement('div', { style: { textAlign: 'right' } }, _react2.default.createElement('img', { className: 'mastersImage', src: '/static/images/masters.svg' }))), _react2.default.createElement(_uikit.Column, { xs: 7 }, _react2.default.createElement(_HeroHeader2.default, { color: 'white', align: 'left', size: '250' }, '2018 Masters Pool'), _react2.default.createElement(_HeroText2.default, { color: 'white', align: 'left', size: '150' }, 'In support of Cystic Fibrosis Canada'), _react2.default.createElement(_HeroText2.default, { color: 'white', align: 'left', size: '150' }, '$10.00 per entry'))), _react2.default.createElement(_uikit.Column, { xs: 12 }, _react2.default.createElement(_uikit.SubHeader, { align: 'left', size: '140' }, 'Welcome'), _react2.default.createElement(_uikit.Text, { align: 'left' }, 'Thank you for your interest in the 2018 Masters Pool. 50% of the total entry fee money will be donated to Cystic Fibrosis Canada in support of research efforts to develop a cure. '), _react2.default.createElement('br', null), _react2.default.createElement(_uikit.SubHeader, { align: 'left', size: '140' }, 'Instructions & Rules'), _react2.default.createElement(List, null, _react2.default.createElement('li', null, 'Select 1 player from each of the 15 groups'), _react2.default.createElement('li', null, 'Your total score will be the sum of all of your players across the groups, less your 2 worst performing players (i.e., 13 best player scores)'), _react2.default.createElement('li', null, '50% of entry fees will be donated to Cystic Fibrosis Canada'), _react2.default.createElement('li', null, 'PRIZING: Of the remaining entry dollars, Top 3 ranked scores get prizes - 1st = 50%, 2nd = 30%, 3rd = 20% of entry purse'), _react2.default.createElement('li', null, 'Deadline for entry is April 4th @ 10:00pm'), _react2.default.createElement('li', null, 'Entry fee: etransfer to Dustin (dustinmalik@gmail.com) or CASH to Doug (doug.arthrell@gmail.com)'), _react2.default.createElement('li', null, 'Check back here (', _react2.default.createElement('a', { href: 'https://masters2018.lineaction.com' }, 'https://masters2018.lineaction.com'), ') after the tournament starts for daily score updates (April 5th to 8th).'), _react2.default.createElement('li', null, 'Winners to be announced on April 9th'), _react2.default.createElement('li', null, 'ONE entry per email address. If you try to submit more entries using the same email it won\'t go through. So for multiple entries use another email address :)'))), _react2.default.createElement(_uikit.Line, null), _react2.default.createElement(_Groups2.default, null)));
});