'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withData = require('../lib/withData');

var _withData2 = _interopRequireDefault(_withData);

var _head = require('next/dist/lib/head.js');

var _head2 = _interopRequireDefault(_head);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

var _App = require('../components/App');

var _App2 = _interopRequireDefault(_App);

var _AddPlayers = require('../components/AddPlayers');

var _AddPlayers2 = _interopRequireDefault(_AddPlayers);

var _Hero = require('../components/Hero');

var _Hero2 = _interopRequireDefault(_Hero);

var _HeroHeader = require('../components/HeroHeader');

var _HeroHeader2 = _interopRequireDefault(_HeroHeader);

var _HeroText = require('../components/HeroText');

var _HeroText2 = _interopRequireDefault(_HeroText);

var _RenderGroup = require('../components/RenderGroup');

var _RenderGroup2 = _interopRequireDefault(_RenderGroup);

var _Groups = require('../components/Groups');

var _Groups2 = _interopRequireDefault(_Groups);

var _Standings = require('../components/Standings');

var _Standings2 = _interopRequireDefault(_Standings);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: bullet;\n  text-align: left;\n'], ['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: bullet;\n  text-align: left;\n']);

var List = _styledComponents2.default.ul(_templateObject, function (props) {
  return props.theme.fontColor;
}, function (props) {
  return props.theme.paragraphFont;
});

exports.default = (0, _withData2.default)(function (props) {
  return _react2.default.createElement('div', null, _react2.default.createElement(_head2.default, null, _react2.default.createElement('title', null, 'Masters Pool 2018'), _react2.default.createElement('meta', { charSet: 'utf-8' }), _react2.default.createElement('meta', { name: 'viewport', content: 'initial-scale=1.0, width=device-width' })), _react2.default.createElement(_App2.default, null, _react2.default.createElement(_Hero2.default, { alignItems: 'center' }, _react2.default.createElement(_uikit.Column, { xs: 5 }, _react2.default.createElement('div', { style: { textAlign: 'right' } }, _react2.default.createElement('img', { className: 'mastersImage', src: '/static/images/masters.svg' }))), _react2.default.createElement(_uikit.Column, { xs: 7 }, _react2.default.createElement(_HeroHeader2.default, { color: 'white', align: 'left', size: '250' }, '2018 Masters Pool'), _react2.default.createElement(_HeroText2.default, { color: 'white', align: 'left', size: '150' }, 'In support of Cystic Fibrosis Canada'), _react2.default.createElement(_HeroText2.default, { color: 'white', align: 'left', size: '150' }, '$10.00 per entry'))), _react2.default.createElement(_uikit.Column, { xs: 12 }, _react2.default.createElement(_uikit.SubHeader, { align: 'left', size: '140' }, 'Welcome'), _react2.default.createElement(_uikit.Text, { align: 'left' }, 'Thank you for your interest in the 2018 Masters Pool. 50% of the total entry fee money will be donated to Cystic Fibrosis Canada in support of research efforts to develop a cure. '), _react2.default.createElement('br', null), _react2.default.createElement(_uikit.SubHeader, { align: 'left', size: '140' }, 'Prizing'), _react2.default.createElement(List, null, _react2.default.createElement('li', null, 'First = $115'), _react2.default.createElement('li', null, 'Second = $70'), _react2.default.createElement('li', null, 'Third = $45'))), _react2.default.createElement(_uikit.Line, null), _react2.default.createElement(_uikit.Column, { xs: 12 }, _react2.default.createElement(_uikit.SubHeader, { align: 'left', size: '140' }, 'Standings'), _react2.default.createElement(List, null, _react2.default.createElement('li', null, 'Click the entry to view players selected!'), _react2.default.createElement('li', null, 'Your total score will be the sum of all of your players across the groups, less your 2 worst performing players (i.e., 13 best player scores)'), _react2.default.createElement('li', null, 'Entry fee can be paid in cash or etransfer to Dustin (dustinmalik@gmail.com) or Jessica (jessicasmalik@gmail.com) or Doug (doug.arthrell@gmail.com)'), _react2.default.createElement('li', null, 'Check back here hourly for updates (', _react2.default.createElement('a', { href: 'https://masters2017.dustinmalik.com' }, 'https://masters2017.dustinmalik.com'), ') (April 6th to 9th)'), _react2.default.createElement('li', null, 'Winners to be announced on April 9th')), _react2.default.createElement(_Standings2.default, null)), _react2.default.createElement('br', null)));
});