'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _withData = require('../lib/withData');

var _withData2 = _interopRequireDefault(_withData);

var _head = require('next/dist/lib/head.js');

var _head2 = _interopRequireDefault(_head);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

var _App = require('../components/App');

var _App2 = _interopRequireDefault(_App);

var _AddPlayers = require('../components/AddPlayers');

var _AddPlayers2 = _interopRequireDefault(_AddPlayers);

var _Hero = require('../components/Hero');

var _Hero2 = _interopRequireDefault(_Hero);

var _HeroHeader = require('../components/HeroHeader');

var _HeroHeader2 = _interopRequireDefault(_HeroHeader);

var _HeroText = require('../components/HeroText');

var _HeroText2 = _interopRequireDefault(_HeroText);

var _RenderGroup = require('../components/RenderGroup');

var _RenderGroup2 = _interopRequireDefault(_RenderGroup);

var _Groups = require('../components/Groups');

var _Groups2 = _interopRequireDefault(_Groups);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: bullet;\n  text-align: left;\n'], ['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: bullet;\n  text-align: left;\n']);

var List = _styledComponents2.default.ul(_templateObject, function (props) {
  return props.theme.fontColor;
}, function (props) {
  return props.theme.paragraphFont;
});

exports.default = (0, _withData2.default)(function (props) {
  return _react2.default.createElement('div', null, _react2.default.createElement(_head2.default, null, _react2.default.createElement('title', null, 'Masters Pool 2018 - Thank you'), _react2.default.createElement('meta', { charSet: 'utf-8' }), _react2.default.createElement('meta', { name: 'viewport', content: 'initial-scale=1.0, width=device-width' })), _react2.default.createElement(_App2.default, null, _react2.default.createElement(_Hero2.default, { alignItems: 'center' }, _react2.default.createElement(_uikit.Column, { sm: 5 }, _react2.default.createElement('div', { style: { textAlign: 'right' } }, _react2.default.createElement('img', { src: '/static/images/masters.svg', width: '180px' }))), _react2.default.createElement(_uikit.Column, { sm: 7 }, _react2.default.createElement(_HeroHeader2.default, { color: 'white', align: 'left', size: '250' }, '2018 Masters Pool'), _react2.default.createElement(_HeroText2.default, { color: 'white', align: 'left', size: '150' }, 'In support of Cystic Fibrosis Canada'), _react2.default.createElement(_HeroText2.default, { color: 'white', align: 'left', size: '150' }, '$10.00 per entry'))), _react2.default.createElement(_uikit.Column, { xs: 12 }, _react2.default.createElement(_uikit.SubHeader, { align: 'center', size: '140' }, 'Thanks for your entry. Good luck!!!'), _react2.default.createElement(_uikit.Text, { align: 'center' }, 'Entry fee: etransfer to Dustin (dustinmalik@gmail.com) or CASH to Doug (doug.arthrell@gmail.com)'), _react2.default.createElement(_uikit.Text, { align: 'center' }, 'Check back here (', _react2.default.createElement('a', { href: 'https://masters2018.lineaction.com' }, 'https://masters2018.lineaction.com'), ') after the tournament starts for daily score updates (April 5th to 8th).'), _react2.default.createElement(_uikit.Text, { align: 'center' }, 'Learn more about our fundraising efforts in support of securing a cure for Cystic Fibrosis. ', _react2.default.createElement('a', { href: 'https://secure.e2rm.com/registrant/mobile/mobileTeamPage.aspx?EventID=228274&LangPref=en-CA&TeamID=797425&Referrer=http%3a%2f%2fwww.cysticfibrosis.ca%2fwalk%2fsearch%3fs%3dTeam%2b24' }, 'Visit our fundraising page'), '.'))));
});