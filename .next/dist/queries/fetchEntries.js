'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  query {\n    allEntries(orderBy: score_ASC) {\n      id\n      name\n      paid\n      pick1\n      pick2\n      pick3\n      pick4\n      pick5\n      pick6\n      pick7\n      pick8\n      pick9\n      pick10\n      pick11\n      pick12\n      pick13\n      pick14\n      pick15\n      score\n    }\n    allPlayers{\n      id\n      firstname\n      lastname\n      playerId\n      score\n    }\n  }\n'], ['\n  query {\n    allEntries(orderBy: score_ASC) {\n      id\n      name\n      paid\n      pick1\n      pick2\n      pick3\n      pick4\n      pick5\n      pick6\n      pick7\n      pick8\n      pick9\n      pick10\n      pick11\n      pick12\n      pick13\n      pick14\n      pick15\n      score\n    }\n    allPlayers{\n      id\n      firstname\n      lastname\n      playerId\n      score\n    }\n  }\n']);

exports.default = (0, _graphqlTag2.default)(_templateObject);