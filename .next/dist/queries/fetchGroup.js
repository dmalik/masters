'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  query($groupID: String!) {\n    allPlayers(\n      filter: {\n        group: $groupID\n      }\n    ) {\n      firstname\n      lastname\n      age\n      country\n      playerId\n      rank\n      img\n    }\n  }\n'], ['\n  query($groupID: String!) {\n    allPlayers(\n      filter: {\n        group: $groupID\n      }\n    ) {\n      firstname\n      lastname\n      age\n      country\n      playerId\n      rank\n      img\n    }\n  }\n']);

exports.default = (0, _graphqlTag2.default)(_templateObject);