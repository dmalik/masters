'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  query {\n   \tallPlayers{\n      id\n      firstname\n      lastname\n      playerId\n      score\n  \t}\n  }\n'], ['\n  query {\n   \tallPlayers{\n      id\n      firstname\n      lastname\n      playerId\n      score\n  \t}\n  }\n']);

exports.default = (0, _graphqlTag2.default)(_templateObject);