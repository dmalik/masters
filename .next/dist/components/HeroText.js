'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: white;\n  margin-top: 0;\n  text-align: left;\n\n  @media (max-width: 500px) {\n      font-size: ', '%;\n      text-align: center;\n  }\n'], ['\n  color: white;\n  margin-top: 0;\n  text-align: left;\n\n  @media (max-width: 500px) {\n      font-size: ', '%;\n      text-align: center;\n  }\n']);

var HeroText = (0, _styledComponents2.default)(_uikit.Text)(_templateObject, function (props) {
  return props.size ? Number(props.size) / 1.3 : '100';
});

exports.default = HeroText;