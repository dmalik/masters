'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('next/dist/lib/router/index.js');

var _index2 = _interopRequireDefault(_index);

var _reactApollo = require('react-apollo');

var _fetchGroup = require('../queries/fetchGroup');

var _fetchGroup2 = _interopRequireDefault(_fetchGroup);

var _RenderGroup = require('./RenderGroup');

var _RenderGroup2 = _interopRequireDefault(_RenderGroup);

var _formSerialize = require('form-serialize');

var _formSerialize2 = _interopRequireDefault(_formSerialize);

var _createEntry = require('../mutations/createEntry');

var _createEntry2 = _interopRequireDefault(_createEntry);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n'], ['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n  background: transparent;\n  text-align: left;\n  color: ', ';\n  font-family: ', ';\n    padding-top: 0;\n  font-weight: bold;\n'], ['\n  background: transparent;\n  text-align: left;\n  color: ', ';\n  font-family: ', ';\n    padding-top: 0;\n  font-weight: bold;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n  padding-bottom: 0;\n  margin-bottom: 0;\n'], ['\n  padding-bottom: 0;\n  margin-bottom: 0;\n']),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(['\n  color: red;\n  text-align: center;\n'], ['\n  color: red;\n  text-align: center;\n']),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(['\n  padding: 1em;\n'], ['\n  padding: 1em;\n']);

var groupIds = [];

//Create an array of 15 for the 15 groups of players we can map through
for (var x = 1; x <= 15; x++) {
  groupIds.push(x.toString());
}

var EntryColumn = (0, _styledComponents2.default)(_uikit.Column)(_templateObject, function (props) {
  return props.theme.fontColor;
}, function (props) {
  return props.theme.paragraphFont;
});

var EntryInput = (0, _styledComponents2.default)(_uikit.Input)(_templateObject, function (props) {
  return props.theme.fontColor;
}, function (props) {
  return props.theme.paragraphFont;
});

var EntryLabel = (0, _styledComponents2.default)(_uikit.Label)(_templateObject2, function (props) {
  return props.theme.fontColor;
}, function (props) {
  return props.theme.paragraphFont;
});

var EntryHeader = (0, _styledComponents2.default)(_uikit.Header)(_templateObject3);

var ErrorText = (0, _styledComponents2.default)(_uikit.Text)(_templateObject4);

var Form = _styledComponents2.default.form(_templateObject5);

var Groups = function (_Component) {
  (0, _inherits3.default)(Groups, _Component);

  function Groups(props) {
    (0, _classCallCheck3.default)(this, Groups);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Groups.__proto__ || (0, _getPrototypeOf2.default)(Groups)).call(this, props));

    _this.renderGroups = function (groupIds) {
      return groupIds.map(function (id) {
        return _react2.default.createElement(_RenderGroup2.default, { groupId: id, key: id });
      });
    };

    _this.validateEmail = function (email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    };

    _this.submitForm = function (event) {
      event.preventDefault();
      var form = document.querySelector('#masterspool-form');
      var formData = (0, _formSerialize2.default)(form, { hash: true });
      var error = false;
      console.log("Submit! - ", formData);
      for (var y = 1; y <= 15; y++) {
        var object = "group" + y;
        if (formData[object] === undefined) {
          console.log("Group Error!");
          _this.setState({ errorMessage: "Submit error! Looks like you missed selecting a player in group " + y + "." });
          error = true;
        }
      }
      if (formData.name === undefined || formData.email === undefined) {
        console.log("Details Error!");
        _this.setState({ errorMessage: "Submit error! You must enter your name and email address. Scroll up and fill those out." });
        error = true;
      }
      if (error) {
        _this.setState({ error: true });
      } else {
        var validEmail = _this.validateEmail(formData.email);
        if (validEmail) {
          _this.setState({ errorMessage: "" });
          _this.setState({ error: false });
          console.log("Do Submission");
          _this.props.mutate({
            variables: {
              "name": formData.name,
              "email": formData.email,
              "pick1": formData.group1,
              "pick2": formData.group2,
              "pick3": formData.group3,
              "pick4": formData.group4,
              "pick5": formData.group5,
              "pick6": formData.group6,
              "pick7": formData.group7,
              "pick8": formData.group8,
              "pick9": formData.group9,
              "pick10": formData.group10,
              "pick11": formData.group11,
              "pick12": formData.group12,
              "pick13": formData.group13,
              "pick14": formData.group14,
              "pick15": formData.group15
            }
          });
          _index2.default.push('/thanks');
        } else {
          console.log("Email Error!");
          _this.setState({ errorMessage: "Submit error! You must enter a valid email address. Scroll up and fix that." });
        }
      }
    };

    _this.state = { name: '', email: '', error: false, errorMessage: "" };
    return _this;
  }

  (0, _createClass3.default)(Groups, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(Form, { id: 'masterspool-form', onSubmit: this.submitForm }, _react2.default.createElement(EntryHeader, { size: '140' }, 'Your Entry Details'), _react2.default.createElement(EntryColumn, { xs: 10, sm: 8, md: 6, lg: 4 }, _react2.default.createElement(_uikit.FormField, null, _react2.default.createElement(EntryLabel, null, 'Full Name'), _react2.default.createElement(EntryInput, { name: 'name', onChange: function onChange(e) {
          _this2.setState({ name: e.target.value });
        }, value: this.state.name })), _react2.default.createElement(_uikit.FormField, null, _react2.default.createElement(EntryLabel, null, 'Email'), _react2.default.createElement(EntryInput, { name: 'email', onChange: function onChange(e) {
          _this2.setState({ email: e.target.value });
        }, value: this.state.email }))), this.renderGroups(groupIds), _react2.default.createElement(_uikit.Row, null, _react2.default.createElement(_uikit.Column, { xs: 12, style: { textAlign: 'center' } }, this.state.error ? _react2.default.createElement(ErrorText, null, ' ', this.state.errorMessage, ' ') : "", _react2.default.createElement(_uikit.PrimaryButton, { size: '125', type: 'submit' }, 'SUBMIT ENTRY'))));
    }
  }]);

  return Groups;
}(_react.Component);

exports.default = (0, _reactApollo.graphql)(_createEntry2.default)(Groups);