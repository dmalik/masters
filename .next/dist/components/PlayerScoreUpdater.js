'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactApollo = require('react-apollo');

require('isomorphic-fetch');

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _fetchPlayers = require('../queries/fetchPlayers');

var _fetchPlayers2 = _interopRequireDefault(_fetchPlayers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  mutation  UpdatePlayer($id: ID!, $score: Int) {\n  updatePlayer(\n    id: $id\n    score: $score\n  ) {\n    id\n  }\n}\n'], ['\n  mutation  UpdatePlayer($id: ID!, $score: Int) {\n  updatePlayer(\n    id: $id\n    score: $score\n  ) {\n    id\n  }\n}\n']);

var PlayerScoreUpdater = function (_Component) {
  (0, _inherits3.default)(PlayerScoreUpdater, _Component);

  function PlayerScoreUpdater(props) {
    (0, _classCallCheck3.default)(this, PlayerScoreUpdater);

    var _this = (0, _possibleConstructorReturn3.default)(this, (PlayerScoreUpdater.__proto__ || (0, _getPrototypeOf2.default)(PlayerScoreUpdater)).call(this, props));

    _this.updateNow = function () {
      var request = _axios2.default.get('/updatedata');
      request.then(function (_ref) {
        var data = _ref.data;

        console.log("Data = ", data.data.player);
        console.log("Props? = ", _this.props);
        var mastersData = data.data.player;
        var score = 0;
        //let result = testers.find(function (o) { return o.first_name === "Scott" && o.display_name2 === "Piercy"; });
        mastersData.map(function (player) {
          var result = _this.props.data.allPlayers.find(function (o) {
            return o.firstname === player.first_name && o.lastname === player.last_name;
          });
          if (result === undefined) {
            console.log("Player Error = ", player);
          } else {
            console.log(result.id + " " + result.firstname + " " + result.lastname + " = " + player.topar);
            if (player.topar === "E") {
              score = 0;
            } else {
              score = Number(player.topar);
            }
            _this.props.mutate({
              variables: {
                "id": result.id,
                "score": score
              }
            });
          }
        });
      }).catch(function (err) {
        console.log("Error = ", errPayload);
      });
    };

    _this.state = {
      players: []
    };
    return _this;
  }

  (0, _createClass3.default)(PlayerScoreUpdater, [{
    key: 'render',
    value: function render() {
      if (this.props.data.loading) {
        return _react2.default.createElement('div', null, 'Loading...');
      }
      return _react2.default.createElement('div', null, _react2.default.createElement(_uikit.Button, { onClick: this.updateNow }, ' UPDATE PLAYER SCORES '));
    }
  }]);

  return PlayerScoreUpdater;
}(_react.Component);

var mutation = (0, _reactApollo.gql)(_templateObject);

exports.default = (0, _reactApollo.graphql)(mutation)((0, _reactApollo.graphql)(_fetchPlayers2.default)(PlayerScoreUpdater));