'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactApollo = require('react-apollo');

require('isomorphic-fetch');

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _reactCollapsible = require('react-collapsible');

var _reactCollapsible2 = _interopRequireDefault(_reactCollapsible);

var _fetchEntries = require('../queries/fetchEntries');

var _fetchEntries2 = _interopRequireDefault(_fetchEntries);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Standings = function (_Component) {
  (0, _inherits3.default)(Standings, _Component);

  function Standings(props) {
    (0, _classCallCheck3.default)(this, Standings);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Standings.__proto__ || (0, _getPrototypeOf2.default)(Standings)).call(this, props));

    _this.renderStangings = function () {
      var place = 0;
      //let parsedEntries = [];
      return _this.props.data.allEntries.map(function (entry) {
        place++;
        var score1 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick1;
        });
        var score2 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick2;
        });
        var score3 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick3;
        });
        var score4 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick4;
        });
        var score5 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick5;
        });
        var score6 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick6;
        });
        var score7 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick7;
        });
        var score8 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick8;
        });
        var score9 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick9;
        });
        var score10 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick10;
        });
        var score11 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick11;
        });
        var score12 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick12;
        });
        var score13 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick13;
        });
        var score14 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick14;
        });
        var score15 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick15;
        });
        /*let picks = [
          { "group": 1, "firstname": score1.firstname, "lastname": score1.lastname, "score": score1.score },
          { "group": 2, "firstname":  score2.firstname, "lastname": score2.lastname, "score": score2.score },
          { "group": 3, "firstname":  score3.firstname, "lastname": score3.lastname, "score": score3.score },
          { "group": 4, "firstname":  score4.firstname, "lastname": score4.lastname, "score": score4.score },
          { "group": 5, "firstname":  score5.firstname, "lastname": score5.lastname, "score": score5.score },
          { "group": 6, "firstname":  score6.firstname, "lastname": score6.lastname, "score": score6.score },
          { "group": 7, "firstname":  score7.firstname, "lastname": score7.lastname, "score": score7.score },
          { "group": 8, "firstname":  score8.firstname, "lastname": score8.lastname, "score": score8.score },
          { "group": 9, "firstname":  score9.firstname, "lastname": score9.lastname, "score": score9.score },
          { "group": 10, "firstname": score10.firstname, "lastname": score10.lastname,"score":  score10.score },
          { "group": 11, "firstname": score11.firstname, "lastname": score11.lastname,"score":  score11.score },
          { "group": 12, "firstname": score12.firstname, "lastname": score12.lastname,"score":  score12.score },
          { "group": 13, "firstname": score13.firstname, "lastname": score13.lastname,"score":  score13.score },
          { "group": 14, "firstname": score14.firstname, "lastname": score14.lastname,"score":  score14.score },
          { "group": 15, "firstname": score15.firstname, "lastname": score15.lastname,"score":  score15.score }
        ];*/

        //parsedEntries.push([place, entry.name, entry.score, picks])
        var title = place + ". " + entry.name + " ( " + entry.score + " )";
        //console.log("picks = ", picks);
        return _react2.default.createElement(_reactCollapsible2.default, { trigger: title, key: entry.id }, _react2.default.createElement('p', null, 'Group 1: ', score1.firstname, ' ', score1.lastname, ' ( ', score1.score, ' )'), _react2.default.createElement('p', null, 'Group 2: ', score2.firstname, ' ', score2.lastname, ' ( ', score2.score, ' )'), _react2.default.createElement('p', null, 'Group 3: ', score3.firstname, ' ', score3.lastname, ' ( ', score3.score, ' )'), _react2.default.createElement('p', null, 'Group 4: ', score4.firstname, ' ', score4.lastname, ' ( ', score4.score, ' )'), _react2.default.createElement('p', null, 'Group 5: ', score5.firstname, ' ', score5.lastname, ' ( ', score5.score, ' )'), _react2.default.createElement('p', null, 'Group 6: ', score6.firstname, ' ', score6.lastname, ' ( ', score6.score, ' )'), _react2.default.createElement('p', null, 'Group 7: ', score7.firstname, ' ', score7.lastname, ' ( ', score7.score, ' )'), _react2.default.createElement('p', null, 'Group 8: ', score8.firstname, ' ', score8.lastname, ' ( ', score8.score, ' )'), _react2.default.createElement('p', null, 'Group 9: ', score9.firstname, ' ', score9.lastname, ' ( ', score9.score, ' )'), _react2.default.createElement('p', null, 'Group 10: ', score10.firstname, ' ', score10.lastname, ' ( ', score10.score, ' )'), _react2.default.createElement('p', null, 'Group 11: ', score11.firstname, ' ', score11.lastname, ' ( ', score11.score, ' )'), _react2.default.createElement('p', null, 'Group 12: ', score12.firstname, ' ', score12.lastname, ' ( ', score12.score, ' )'), _react2.default.createElement('p', null, 'Group 13: ', score13.firstname, ' ', score13.lastname, ' ( ', score13.score, ' )'), _react2.default.createElement('p', null, 'Group 14: ', score14.firstname, ' ', score14.lastname, ' ( ', score14.score, ' )'), _react2.default.createElement('p', null, 'Group 15: ', score15.firstname, ' ', score15.lastname, ' ( ', score15.score, ' )'));
      });
    };

    _this.state = {
      players: []
    };
    return _this;
  }

  (0, _createClass3.default)(Standings, [{
    key: 'render',
    value: function render() {
      if (this.props.data.loading) {
        return _react2.default.createElement('div', null, 'Loading...');
      }
      return _react2.default.createElement('div', null, this.renderStangings());
    }
  }]);

  return Standings;
}(_react.Component);

exports.default = (0, _reactApollo.graphql)(_fetchEntries2.default)(Standings);