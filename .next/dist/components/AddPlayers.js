'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactApollo = require('react-apollo');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  mutation createPlayer(\n  $playerId: String!,\n  $age: String,\n  $country: String,\n  $firstname: String,\n  $group: String,\n  $img: String,\n  $lastname: String,\n  $rank: String,\n  $score: Int) {\n  createPlayer(\n    playerId: $playerId,\n    age: $age,\n    country: $country,\n  \tfirstname: $firstname,\n  \tgroup: $group,\n  \timg: $img,\n  \tlastname: $lastname,\n  \trank: $rank,\n  \tscore: $score){\n      firstname\n    }\n}\n'], ['\n  mutation createPlayer(\n  $playerId: String!,\n  $age: String,\n  $country: String,\n  $firstname: String,\n  $group: String,\n  $img: String,\n  $lastname: String,\n  $rank: String,\n  $score: Int) {\n  createPlayer(\n    playerId: $playerId,\n    age: $age,\n    country: $country,\n  \tfirstname: $firstname,\n  \tgroup: $group,\n  \timg: $img,\n  \tlastname: $lastname,\n  \trank: $rank,\n  \tscore: $score){\n      firstname\n    }\n}\n']);
//Include this in one of the pages to create initial player list

var playerData = require('../initialData/players2018.json');

function AddPlayers(_ref) {
  var createPlayer = _ref.createPlayer;

  function handleSubmit(e) {
    e.preventDefault();

    playerData.players.map(function (player) {
      console.log("player = ", player);
      createPlayer(player.playerId, player.age, player.country, player.firstname, player.group, player.img, player.lastname, player.rank, player.score);
    });
  }

  return _react2.default.createElement('form', { onSubmit: handleSubmit }, _react2.default.createElement('h1', null, 'Add Players'), _react2.default.createElement('button', { type: 'submit' }, 'Add Players'));
}

var createPlayer = (0, _reactApollo.gql)(_templateObject);

exports.default = (0, _reactApollo.graphql)(createPlayer, {
  props: function props(_ref2) {
    var mutate = _ref2.mutate;
    return {
      createPlayer: function createPlayer(playerId, age, country, firstname, group, img, lastname, rank, score) {
        return mutate({
          variables: { playerId: playerId, age: age, country: country, firstname: firstname, group: group, img: img, lastname: lastname, rank: rank, score: score },
          updateQueries: {
            allPlayers: function allPlayers(previousResult, _ref3) {
              var mutationResult = _ref3.mutationResult;

              var newPlayer = mutationResult.data.createPost;
              return (0, _assign2.default)({}, previousResult, {
                allPosts: [newPlayer].concat((0, _toConsumableArray3.default)(previousResult.allPlayers))
              });
            }
          }
        });
      }
    };
  }
})(AddPlayers);