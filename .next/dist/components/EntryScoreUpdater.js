'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactApollo = require('react-apollo');

require('isomorphic-fetch');

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _fetchEntries = require('../queries/fetchEntries');

var _fetchEntries2 = _interopRequireDefault(_fetchEntries);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  mutation  UpdateEntry($id: ID!, $score: Int) {\n    updateEntry(\n      id: $id\n      score: $score\n    ) {\n      id\n    }\n  }\n'], ['\n  mutation  UpdateEntry($id: ID!, $score: Int) {\n    updateEntry(\n      id: $id\n      score: $score\n    ) {\n      id\n    }\n  }\n']);

var EntryScoreUpdater = function (_Component) {
  (0, _inherits3.default)(EntryScoreUpdater, _Component);

  function EntryScoreUpdater(props) {
    (0, _classCallCheck3.default)(this, EntryScoreUpdater);

    var _this = (0, _possibleConstructorReturn3.default)(this, (EntryScoreUpdater.__proto__ || (0, _getPrototypeOf2.default)(EntryScoreUpdater)).call(this, props));

    _this.updateNow = function () {
      _this.props.data.allEntries.map(function (entry) {
        var score1 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick1;
        });
        var score2 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick2;
        });
        var score3 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick3;
        });
        var score4 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick4;
        });
        var score5 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick5;
        });
        var score6 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick6;
        });
        var score7 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick7;
        });
        var score8 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick8;
        });
        var score9 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick9;
        });
        var score10 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick10;
        });
        var score11 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick11;
        });
        var score12 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick12;
        });
        var score13 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick13;
        });
        var score14 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick14;
        });
        var score15 = _this.props.data.allPlayers.find(function (o) {
          return o.playerId === entry.pick15;
        });
        var scores = [score1.score, score2.score, score3.score, score4.score, score5.score, score6.score, score7.score, score8.score, score9.score, score10.score, score11.score, score12.score, score13.score, score14.score, score15.score];
        //console.log(entry.name + " = ", scores);
        function compareNumbers(a, b) {
          return a - b;
        }
        scores.sort(compareNumbers);
        var totalScore = 0;
        for (var x = 0; x <= 12; x++) {
          totalScore = scores[x] + totalScore;
        }
        console.log(entry.name + " = " + totalScore);
        _this.props.mutate({
          variables: {
            "id": entry.id,
            "score": totalScore
          }
        });
      });
    };

    _this.state = {
      players: []
    };
    return _this;
  }

  (0, _createClass3.default)(EntryScoreUpdater, [{
    key: 'render',
    value: function render() {
      if (this.props.data.loading) {
        return _react2.default.createElement('div', null, 'Loading...');
      }
      return _react2.default.createElement('div', null, _react2.default.createElement(_uikit.Button, { onClick: this.updateNow }, ' UPDATE ENTRY SCORES '));
    }
  }]);

  return EntryScoreUpdater;
}(_react.Component);

var mutation = (0, _reactApollo.gql)(_templateObject);

exports.default = (0, _reactApollo.graphql)(mutation)((0, _reactApollo.graphql)(_fetchEntries2.default)(EntryScoreUpdater));