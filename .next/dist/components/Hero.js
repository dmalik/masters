'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: white;\n  height: 40vh;\n  min-height: 20em;\n  background-image: url(\'static/images/augusta3.jpg\');\n  background-repeat: no-repeat;\n  background-attachment: fixed;\n  background-color: ', ';\n'], ['\n  color: white;\n  height: 40vh;\n  min-height: 20em;\n  background-image: url(\'static/images/augusta3.jpg\');\n  background-repeat: no-repeat;\n  background-attachment: fixed;\n  background-color: ', ';\n']);

var Hero = (0, _styledComponents2.default)(_uikit.Row)(_templateObject, function (props) {
  return props.theme.primaryColor ? props.theme.primaryColor : '#4990E2';
});

exports.default = Hero;