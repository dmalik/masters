'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: white;\n  text-align: left;\n\n  @media (max-width: 500px) {\n      font-size: ', '%;\n      text-align: center;\n  }\n\n'], ['\n  color: white;\n  text-align: left;\n\n  @media (max-width: 500px) {\n      font-size: ', '%;\n      text-align: center;\n  }\n\n']);

var HeroHeader = (0, _styledComponents2.default)(_uikit.Header)(_templateObject, function (props) {
  return props.size ? Number(props.size) / 1.5 : '100';
});

exports.default = HeroHeader;