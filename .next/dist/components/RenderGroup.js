'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _taggedTemplateLiteral2 = require('babel-runtime/helpers/taggedTemplateLiteral');

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactApollo = require('react-apollo');

var _fetchGroup = require('../queries/fetchGroup');

var _fetchGroup2 = _interopRequireDefault(_fetchGroup);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

var _uikit = require('../uikit');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _templateObject = (0, _taggedTemplateLiteral3.default)(['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: none;\n  text-align: center;\n  margin: 1.66666666666666%;\n  border-radius: 3px;\n  transition: box-shadow .25s;\n  border-radius: 2px;\n  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\n  cursor: pointer;\n  transition: all 0.25s linear;\n  > label {\n    cursor: pointer;\n  }\n'], ['\n  color: ', ';\n  font-family: ', ';\n  font-size: 16px;\n  list-style-type: none;\n  text-align: center;\n  margin: 1.66666666666666%;\n  border-radius: 3px;\n  transition: box-shadow .25s;\n  border-radius: 2px;\n  box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\n  cursor: pointer;\n  transition: all 0.25s linear;\n  > label {\n    cursor: pointer;\n  }\n']),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(['\n  margin-top: 1em;\n  margin-bottom: 0;\n'], ['\n  margin-top: 1em;\n  margin-bottom: 0;\n']),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(['\n  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.14), 0 1px 3px 0 rgba(0,0,0,0.12), 0 2px 1px -2px rgba(0,0,0,0.2);\n'], ['\n  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.14), 0 1px 3px 0 rgba(0,0,0,0.12), 0 2px 1px -2px rgba(0,0,0,0.2);\n']);

var PlayerColumn = (0, _styledComponents2.default)(_uikit.Column)(_templateObject, function (props) {
  return props.theme.fontColor;
}, function (props) {
  return props.theme.paragraphFont;
});

var GroupHeader = (0, _styledComponents2.default)(_uikit.Header)(_templateObject2);

var Flag = _styledComponents2.default.img(_templateObject3);

var RenderGroup = function (_Component) {
  (0, _inherits3.default)(RenderGroup, _Component);

  function RenderGroup() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, RenderGroup);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = RenderGroup.__proto__ || (0, _getPrototypeOf2.default)(RenderGroup)).call.apply(_ref, [this].concat(args))), _this), _this.playerClicked = function (e) {
      if (e.target.value != undefined) {
        console.log("Click - ", e.target.value);
      }
    }, _this.groupRender = function (item) {
      return item.map(function (player) {
        return _react2.default.createElement(PlayerColumn, { fluid: true, xs: 5, sm: 5, md: 2, key: player.playerId, onClick: _this.playerClicked }, _react2.default.createElement('input', { type: 'radio', name: "group" + _this.props.groupId, value: player.playerId, id: player.playerId }), _react2.default.createElement('label', { htmlFor: player.playerId }, _react2.default.createElement('img', { src: "/static/images/players/" + player.img, width: '100%', style: { paddingBottom: "0.3em" } }), _react2.default.createElement(Flag, { src: "/static/images/flags/" + player.country + ".svg", width: '15%', style: { marginBottom: "0.3em" } }), _react2.default.createElement('div', null, player.firstname, ' ', player.lastname), _react2.default.createElement('div', null, 'Age: ', player.age)));
      });
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(RenderGroup, [{
    key: 'render',
    value: function render() {
      if (this.props.loading) {
        return _react2.default.createElement('div', null, 'Loading');
      }

      return _react2.default.createElement(_uikit.Row, null, _react2.default.createElement(_uikit.Column, { fluid: true, xs: 12 }, _react2.default.createElement(GroupHeader, { size: '140' }, 'Group ', this.props.groupId)), this.groupRender(this.props.data.allPlayers));
    }
  }]);

  return RenderGroup;
}(_react.Component);

exports.default = (0, _reactApollo.graphql)(_fetchGroup2.default, {
  options: function options(props) {
    return { variables: { groupID: props.groupId } };
  }
})(RenderGroup);