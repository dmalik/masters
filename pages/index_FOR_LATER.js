import React, {Component} from 'react';
import withData from '../lib/withData';
import Head from 'next/head';
import styled from 'styled-components';
import {Default,Button,PrimaryButton,Header,SubHeader,Page,Row,Column,Gradient,Line,Text} from '../uikit';

import App from '../components/App';
import AddPlayers from '../components/AddPlayers';
import Hero from '../components/Hero';
import HeroHeader from '../components/HeroHeader';
import HeroText from '../components/HeroText';
import RenderGroup from '../components/RenderGroup';
import Groups from '../components/Groups';
import Standings from '../components/Standings';

const List = styled.ul `
  color: ${props => props.theme.fontColor};
  font-family: ${props => props.theme.paragraphFont};
  font-size: 16px;
  list-style-type: bullet;
  text-align: left;
`;

export default withData((props) => (
  <div>
    <Head>
      <title>Masters Pool 2018</title>
      <meta charSet='utf-8'/>
      <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
    </Head>
    <App>
      <Hero alignItems='center'>
        <Column xs={5}>
          <div style={{textAlign: 'right'}}>
            <img className="mastersImage" src="/static/images/masters.svg"/>
          </div>
        </Column>
        <Column xs={7}>
          <HeroHeader color='white' align='left' size='250'>2018 Masters Pool</HeroHeader>
          <HeroText color='white' align='left' size='150'>In support of Cystic Fibrosis Canada</HeroText>
          <HeroText color='white' align='left' size='150'>$10.00 per entry</HeroText>
        </Column>
      </Hero>
      <Column xs={12}>
        <SubHeader align='left' size='140'>Welcome</SubHeader>
        <Text align='left'>Thank you for your interest in the 2018 Masters Pool. 50% of the total entry fee money will be donated to Cystic Fibrosis Canada in support of research efforts to develop a cure. </Text>
        <br/>
        <SubHeader align='left' size='140'>Prizing</SubHeader>
        <List>
          <li>First = $115</li>
          <li>Second = $70</li>
          <li>Third = $45</li>
        </List>
      </Column>
      <Line/>
      <Column xs={12}>
        <SubHeader align='left' size='140'>Standings</SubHeader>
        <List>
          <li>Click the entry to view players selected!</li>
          <li>Your total score will be the sum of all of your players across the groups, less your 2 worst performing players (i.e., 13 best player scores)</li>
          <li>Entry fee can be paid in cash or etransfer to Dustin (dustinmalik@gmail.com) or Jessica (jessicasmalik@gmail.com) or Doug (doug.arthrell@gmail.com)</li>
          <li>Check back here hourly for updates (<a href="https://masters2017.dustinmalik.com">https://masters2017.dustinmalik.com</a>) (April 6th to 9th)</li>
          <li>Winners to be announced on April 9th</li>
        </List>
        <Standings />
      </Column>
      <br/>
    </App>
  </div>
));
