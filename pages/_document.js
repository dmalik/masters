import Document, { Head, Main, NextScript } from 'next/document';
import styleSheet from 'styled-components/lib/models/StyleSheet';
import styled, { ThemeProvider } from 'styled-components';
import {Default, Button, PrimaryButton, Header, SubHeader, Page, Row, Column, Gradient, Line, Text} from '../uikit';


export default class MyDocument extends Document {
  static async getInitialProps ({ renderPage }) {
    const page = renderPage()
    const styles = (
      <style dangerouslySetInnerHTML={{ __html: styleSheet.rules().map(rule => rule.cssText).join('\n') }} />
    )
    return { ...page, styles }
  }

  render () {
    return (
     <html>
       <Head>
        <meta name="robots" content="noindex, nofollow"/>
        <link href="https://fonts.googleapis.com/css?family=Nunito:600|Poppins:600" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="./static/index.css"/>
        <link rel="stylesheet" type="text/css" href="./static/trophy.css"/>
       </Head>
       <body>
         <Main />
         <NextScript />
       </body>
     </html>
   );
  }
}
