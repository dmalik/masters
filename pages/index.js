import React, {Component} from 'react';
import withData from '../lib/withData';
import Head from 'next/head';
import styled from 'styled-components';
import {Default,Button,PrimaryButton,Header,SubHeader,Page,Row,Column,Gradient,Line,Text} from '../uikit';

import App from '../components/App';
import AddPlayers from '../components/AddPlayers';
import Hero from '../components/Hero';
import HeroHeader from '../components/HeroHeader';
import HeroText from '../components/HeroText';
import RenderGroup from '../components/RenderGroup';
import Groups from '../components/Groups';

const List = styled.ul `
  color: ${props => props.theme.fontColor};
  font-family: ${props => props.theme.paragraphFont};
  font-size: 16px;
  list-style-type: bullet;
  text-align: left;
`;

export default withData((props) => (
  <div>
    <Head>
      <title>Masters Pool 2018</title>
      <meta charSet='utf-8'/>
      <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
    </Head>
    <App>
      <Hero alignItems='center'>
        <Column xs={5}>
          <div style={{textAlign: 'right'}}>
            <img className="mastersImage" src="/static/images/masters.svg"/>
          </div>
        </Column>
        <Column xs={7}>
          <HeroHeader color='white' align='left' size='250'>2018 Masters Pool</HeroHeader>
          <HeroText color='white' align='left' size='150'>In support of Cystic Fibrosis Canada</HeroText>
          <HeroText color='white' align='left' size='150'>$10.00 per entry</HeroText>
        </Column>
      </Hero>
      <Column xs={12}>
        <SubHeader align='left' size='140'>Welcome</SubHeader>
        <Text align='left'>Thank you for your interest in the 2018 Masters Pool. 50% of the total entry fee money will be donated to Cystic Fibrosis Canada in support of research efforts to develop a cure. </Text>
        <br/>
        <SubHeader align='left' size='140'>Instructions & Rules</SubHeader>
        <List>
          <li>Select 1 player from each of the 15 groups</li>
          <li>Your total score will be the sum of all of your players across the groups, less your 2 worst performing players (i.e., 13 best player scores)</li>
          <li>50% of entry fees will be donated to Cystic Fibrosis Canada</li>
          <li>PRIZING: Of the remaining entry dollars, Top 3 ranked scores get prizes - 1st = 50%, 2nd = 30%, 3rd = 20% of entry purse</li>
          <li>Deadline for entry is April 4th @ 10:00pm</li>
          <li>Entry fee: etransfer to Dustin (dustinmalik@gmail.com) or CASH to Doug (doug.arthrell@gmail.com)</li>
          <li>Check back here (<a href="https://masters2018.lineaction.com">https://masters2018.lineaction.com</a>) after the tournament starts for daily score updates (April 5th to 8th).</li>
          <li>Winners to be announced on April 9th</li>
          <li>ONE entry per email address. If you try to submit more entries using the same email it won't go through. So for multiple entries use another email address :)</li>
        </List>
      </Column>
      <Line/>
      <Groups />
    </App>
  </div>
));
