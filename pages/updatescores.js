import React, {Component} from 'react';
import withData from '../lib/withData';
import Head from 'next/head';
import styled from 'styled-components';
import { Default,Button,PrimaryButton,Header,SubHeader,Page,Row,Column,Gradient,Line,Text } from '../uikit';

import App from '../components/App';
import PlayerScoreUpdater from '../components/PlayerScoreUpdater';
import EntryScoreUpdater from '../components/EntryScoreUpdater';
import 'isomorphic-fetch';

export default withData((props) => (
      <div>
        <Head>
          <title>Masters Pool 2018 - Update Scores</title>
          <meta charSet='utf-8'/>
          <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
        </Head>
        <App>
          <Column xs={12}>
            <PlayerScoreUpdater />
            <EntryScoreUpdater />
          </Column>
        </App>
      </div>
));
