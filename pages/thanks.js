import React, {Component} from 'react';
import withData from '../lib/withData';
import Head from 'next/head';
import styled from 'styled-components';
import {
  Default,
  Button,
  PrimaryButton,
  Header,
  SubHeader,
  Page,
  Row,
  Column,
  Gradient,
  Line,
  Text
} from '../uikit';

import App from '../components/App';
import AddPlayers from '../components/AddPlayers';
import Hero from '../components/Hero';
import HeroHeader from '../components/HeroHeader';
import HeroText from '../components/HeroText';
import RenderGroup from '../components/RenderGroup';
import Groups from '../components/Groups';

const List = styled.ul `
  color: ${props => props.theme.fontColor};
  font-family: ${props => props.theme.paragraphFont};
  font-size: 16px;
  list-style-type: bullet;
  text-align: left;
`;

export default withData((props) => (
  <div>
    <Head>
      <title>Masters Pool 2018 - Thank you</title>
      <meta charSet='utf-8'/>
      <meta name='viewport' content='initial-scale=1.0, width=device-width'/>
    </Head>
    <App>
      <Hero alignItems='center'>
        <Column sm={5}>
          <div style={{textAlign: 'right'}}>
            <img src="/static/images/masters.svg" width="180px"/>
          </div>
        </Column>
        <Column sm={7}>
          <HeroHeader color='white' align='left' size='250'>2018 Masters Pool</HeroHeader>
          <HeroText color='white' align='left' size='150'>In support of Cystic Fibrosis Canada</HeroText>
          <HeroText color='white' align='left' size='150'>$10.00 per entry</HeroText>
        </Column>
      </Hero>
      <Column xs={12}>
        <SubHeader align='center' size='140'>Thanks for your entry. Good luck!!!</SubHeader>
        <Text align='center'>Entry fee: etransfer to Dustin (dustinmalik@gmail.com) or CASH to Doug (doug.arthrell@gmail.com)</Text>
        <Text align='center'>Check back here (<a href="https://masters2018.lineaction.com">https://masters2018.lineaction.com</a>) after the tournament starts for daily score updates (April 5th to 8th).</Text>
        <Text align='center'>Learn more about our fundraising efforts in support of securing a cure for Cystic Fibrosis. <a href="https://secure.e2rm.com/registrant/mobile/mobileTeamPage.aspx?EventID=228274&LangPref=en-CA&TeamID=797425&Referrer=http%3a%2f%2fwww.cysticfibrosis.ca%2fwalk%2fsearch%3fs%3dTeam%2b24">Visit our fundraising page</a>.</Text>
      </Column>
    </App>
  </div>
));
