const express = require('express');
const next = require('next');
const request = require('request');
/*const cheerio = require('cheerio');*/

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare()
.then(() => {
  const server = express()

  server.get('/thanks', (req, res) => {
    return app.render(req, res, '/thanks', req.query)
  });

  server.get('/updatescores', (req, res) => {
    return app.render(req, res, '/updatescores', req.query)
  });

  server.get('/updatedata', (req, res) => {
    request('http://www.masters.com/en_US/scores/feeds/scores.json', function (error, response, html) {
      if (!error && response.statusCode == 200) {
        res.setHeader('Content-Type', 'application/json');
        res.send(html);
      res.end(html);
      } else {
        return (console.log("error!! - ", error) );
        res.send(html);
      }
    });
  });

  server.get('*', (req, res) => {
    return handle(req, res)
  });

  server.listen(3000, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:3000')
  });
});
